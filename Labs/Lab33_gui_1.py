#!/usr/bin/env python

"""
Lab33 - Initial lab on Graphical User Interfaces (GUIs)
"""
# =====================================================================

import tkinter as tk
from tkinter import ttk

# Metadata
__author__ = "Jake Magness"

'''
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
It is assumed that you have read the readings for lesson 33. If you have
not, please read them before attempting this lab exercise.
'''
# ---------------------------------------------------------------------
# Exercise 1:
# The reading on " Standard Dialog Boxes" provides a description of the
# various pre-defined gui interfaces built into tkinter. In PyCharm,
# open a "Python Console" and then experiment with the various dialog
# boxes. Note that you have to import the required tk module into the
# "Python Console" so that the appropriate objects are defined.
#
# Note: Some of the dialog boxes require an "application window" and
#       if one does not exist, it will automatically create one. For
#       this reason you will often see two windows appear on your screen.
#
#       If the application window is hidden from view, the dialog box
#       will be hidden too. If you execute a dialog box and it does
#       not show up on the screen, it is probably hidden behind other
#       windows. You can click on its icon in the task bar to bring it
#       to the front (which makes it visible.)
#
#       The default application window often gets "hung" because it has
#       no functionality and it does not know how to quit. You sometimes
#       need to kill the "Python Console" by hitting the red "X" in the
#       left tools panel of the "Python Console" and open a new console.
#
# After you have experimented, copy and paste a few of your tests into
# the test_gui_dialog() function below.
# ---------------------------------------------------------------------


def test_gui_dialogs():
    window = tk.Tk()
    window['padx'] = 200
    window['pady'] = 200

    cmd_frame = ttk.LabelFrame(window, text="Commands", relief=tk.RIDGE)
    cmd_frame.grid(row=20, column=20, sticky=tk.E + tk.W + tk.N + tk.S)

    # window1 = tk.Tk()
    window.mainloop()
    pass

# ---------------------------------------------------------------------
# Exercise 2:
# In each of the functions below, create a gui window that looks like
# the examples shown on the Lab 33 web page on the Moodle server. It is
# recommended that you start with the "Hello World" program in the "GUI
# Introduction" lesson as your starting point. To see examples of the
# various widgets, you can examine the code in the all_user_input_widgets.py
# and all_frame_widgets.py example programs from the readings.
#
# Note: You are only creating the "interface." Your GUI windows will
#       have no functionality. We will add the functionality behind
#       the widgets in future lessons.
# ---------------------------------------------------------------------


def interface1():
    import tkinter as tk
    from tkinter import ttk

    # Create the application window
    window = tk.Tk()

    # Create the user interface
    my_label = ttk.Label(window, text="This is a test!")
    my_label.grid(row=1, column=0)

    # Create a button
    frame1 = ttk.Frame(window, relief=tk.RIDGE)
    button1 = ttk.Button(window, text="do task ")
    button1.grid(row=3, column=0)

    # Start the GUI event loop
    window.mainloop()



def interface2():
    import tkinter as tk
    from tkinter import ttk

    # Create the application window
    window = tk.Tk()


    # Data entry line
    entry_label = ttk.Label(window, text="First Name:")
    entry_label.grid(row=1, column=1, sticky=tk.W + tk.N)

    my_first_name = ttk.Entry(window, width=40)
    my_first_name.grid(row=1, column=2, sticky=tk.W, pady=3)

    entry_label1 = ttk.Label(window, text="Last Name:")
    entry_label1.grid(row=2, column=1, sticky=tk.W + tk.N)

    my_last_name = ttk.Entry(window, width=40)
    my_last_name.grid(row=2, column=2, sticky=tk.W + tk.N)

    entry_label3 = ttk.Label(window, text="Address:")
    entry_label3.grid(row=3, column=1, sticky=tk.W + tk.N)

    my_address = ttk.Entry(window, width=40)
    my_address.grid(row=3, column=2, sticky=tk.W + tk.N)



    # Button
    # button = ttk.Button(window, text="Cancel")
    # button.grid(row=4, column=1)
    #
    # button1 = ttk.Button(window, text="Apply")
    # button1.grid(row=4, column=2)

    # Start the GUI event loop
    window.mainloop()


def interface3():
    import tkinter as tk
    from tkinter import ttk

    # Create the application window
    window = tk.Tk()


    # Data entry line
    entry_label = ttk.Label(window, text="First Name:")
    entry_label.grid(row=1, column=1, sticky=tk.W + tk.N)

    my_first_name = ttk.Entry(window, width=40)
    my_first_name.grid(row=1, column=2, sticky=tk.W, pady=3)

    entry_label1 = ttk.Label(window, text="Last Name:")
    entry_label1.grid(row=2, column=1, sticky=tk.W + tk.N)

    my_last_name = ttk.Entry(window, width=40)
    my_last_name.grid(row=2, column=2, sticky=tk.W + tk.N)

    entry_label3 = ttk.Label(window, text="Address:")
    entry_label3.grid(row=3, column=1, sticky=tk.W + tk.N)

    my_address = ttk.Entry(window, width=40)
    my_address.grid(row=3, column=2, sticky=tk.W + tk.N)



    # Button
    button = ttk.Button(window, text="Cancel")
    button.grid(row=4, column=1)

    button1 = ttk.Button(window, text="Apply")
    button1.grid(row=4, column=2)

    # Start the GUI event loop
    window.mainloop()



def interface4():
    import tkinter as tk
    from tkinter import ttk

    # Create the application window
    window = tk.Tk()

    window['padx'] = 20
    window['pady'] = 20

    # Frame the PERSONAL section
    personal_frame = ttk.LabelFrame(window, text="Personal Data", relief=tk.RIDGE)
    personal_frame.grid(row=0, column=0, sticky=tk.E + tk.W + tk.N + tk.S)

    # Data entry line
    entry_label = ttk.Label(personal_frame, text="First Name:")
    entry_label.grid(row=2, column=1, sticky=tk.W + tk.N)

    my_first_name = ttk.Entry(personal_frame, width=20)
    my_first_name.grid(row=2, column=2, sticky=tk.W, pady=4)

    entry_label1 = ttk.Label(personal_frame, text="Last Name:")
    entry_label1.grid(row=3, column=1, sticky=tk.W + tk.N)

    my_last_name = ttk.Entry(personal_frame, width=20)
    my_last_name.grid(row=3, column=2, sticky=tk.W + tk.N)

    entry_label3 = ttk.Label(personal_frame, text="Address:")
    entry_label3.grid(row=4, column=1, sticky=tk.W + tk.N)

    my_address = ttk.Entry(personal_frame, width=20)
    my_address.grid(row=4, column=2, sticky=tk.W + tk.N)


    # Frame other information
    other_frame = ttk.LabelFrame(window, text="Other information", relief=tk.RIDGE)
    other_frame.grid(row=0, column=1, sticky=tk.E + tk.W + tk.N + tk.S)

    # Slider
    scale_label = ttk.Label(other_frame, text="tk.Scale")
    scale_label.grid(row=0, column=1, sticky=tk.N)

    my_scale = tk.Scale(other_frame, from_=0, to=20, orient=tk.HORIZONTAL,
                        width=15, length=100)
    my_scale.grid(row=0, column=1, sticky=tk.N)

    # Checkbox
    # checkbox_label = ttk.Label(other_frame, text="ttk.Checkbutton")
    # checkbox_label.grid(row=1, rowspan=3, column=1, sticky=tk.W + tk.N)

    checkbutton1 = ttk.Checkbutton(other_frame, text="I like Python")
    checkbutton1.grid(row=1, column=1, sticky=tk.W)

    # Button
    button = ttk.Button(window, text="Cancel")
    button.grid(row=0, column=2)

    button1 = ttk.Button(window, text="Apply")
    button1.grid(row=1, column=2, sticky=tk.N)

    # Start the GUI event loop
    window.mainloop()





# ---------------------------------------------------------------------
# If you have extra time, please work on PEX 4.
# ---------------------------------------------------------------------


def main():
    # test_gui_dialogs()
    # interface1()
    # interface2()
    # interface3()
    interface4()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
