#!/usr/bin/env python

"""
Lab09 - Work through the problem solving steps to design, build, and test
        the draw_US_flag() function that uses Turtle Graphics to display a
        scaled American flag drawn to the precise U.S. Code specifications.
"""
# =====================================================================

# Module imports
import math
from turtle import TurtleScreen, RawTurtle, TK

# Metadata
__author__ = "Jake  Magness"
__date__ = "Sept 6, 2016"

# Constants
CANVAS_WIDTH = 1200
CANVAS_HEIGHT = 675
WINDOW_TITLE = "Lab 9 - Problem Solving"
CANVAS_BACKGROUND_COLOR = "light gray"

def create_window():
    """
    Create a graphics window, a drawing canvas in the window, and a turtle
    to draw on the canvas.
    :return: a graphics window, a canvas, and a turtle
    """

    # Create a Tkinter graphics window
    graphics_window = TK.Tk()
    graphics_window.title(WINDOW_TITLE)

    # Create a "canvas" inside the graphics window to draw on
    my_canvas = TK.Canvas(graphics_window, width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
    my_canvas.pack()  # Organizes my_canvas inside the graphics window

    # Create a "canvas" made specifically for turtle graphics
    turtle_canvas = TurtleScreen(my_canvas)
    turtle_canvas.bgcolor(CANVAS_BACKGROUND_COLOR)

    # Create a turtle to draw on the canvas
    my_turtle = RawTurtle(turtle_canvas)

    # Make the turtle draw as fast as possible - comment these lines out for
    # slower rendering.
    turtle_canvas.delay(0)
    my_turtle.hideturtle()
    my_turtle.speed("fastest")

    return graphics_window, my_canvas, my_turtle

# ----------------------------------------------------------------------
# Part 1 - Understand the Problem
# Read completely the Lab 9 handout and the US Code specifications of
# the American Flag as summarized on Wikipedia at:
#    https://en.wikipedia.org/wiki/Flag_of_the_United_States#Specifications, and
#    https://en.wikipedia.org/wiki/Flag_of_the_United_States#Colors.
#
# Part 2 - Design a Solution to the Problem
# Before writing any code, complete the design requirements on the back of
# the handout.  Time spent on developing a good design will save you time
# while coding.
#
# Part 3 - Build the Solution using Python below in this file
#   a. Incrementally develop and test functions used to simplify draw_usa_flag() below.
#   b. Incrementally develop and test the draw_usa_flag() function below.
# DO NOT CHANGE the function header (specification) of draw_usa_flag().
#
# Part 4 - Test and Improve Your Solution
# Write at least three calls in main() to test your draw_usa_flag() with different values.
#
# ----------------------------------------------------------------------

# Part 3.a - place your support function code here

def draw_rectangle(t, height, length, color):
    """

    :param t: the turtle object to use
    :param height: the height in pixels of the rectangle
    :param color: the turtle you wish to use
    :return:
    """

    t.pendown()
    t.begin_fill()
    t.pencolor(color)
    t.fillcolor(color)

    for i in range (2):
        t.forward(length)
        t.right(90)
        t.forward(height)
        t.right(90)

    t.end_fill()
    t.penup()



def draw_star(t, width):
    t.pendown()
    t.fillcolor("white")
    t.pencolor("white")
    t.begin_fill()

    for i in range(6):
        t.forward(width)
        t.right(144)

    t.end_fill()
    t.setheading(0)
# ----------------------------------------------------------------------

# Part 3.b - add code below to implement the provided specification of draw_usa_flag()

def draw_usa_flag(t, x, y, height):
    """
    Draw an American flag at (x,y) with the given height in pixels
    :param t: the turtle used to draw the flax
    :param x: x coordinate in pixels of the upper left corner of the flag
    :param y: y coordinate in pixels of the upper left corner of the flag
    :param height: height (hoist) of the flag in pixels
    :return: NONE
    """

    z = y
    cantonh = height*(7/13)
    cantonw = height*1.9*(2/5)
    stripeh = height/13
    stripew = height*1.9
    stard = stripeh*(4/5)

    # Draws the background of the flag
    t.penup()
    t.goto(x, y)
    draw_rectangle(t, height, stripew, "white")

    # Stripes of the flag
    t.goto(x, y)
    for stripes in range (7):
        draw_rectangle(t, stripeh, stripew, "red")

        z = z -(height/13)*2
        t.goto(x, z)
    # Blue canton section of the flag
    t.penup()
    t.goto(x, y)
    draw_rectangle(t, cantonh ,cantonw, "blue")

    # Draw Stars
    t.penup()
    t.goto(x + (cantonw / 24), y - cantonh / 10)

    for stars in range(6):
        t.pendown()
        draw_star(t, stard)
        t.penup()
        t.forward(cantonw/12)

    t.goto(x + (cantonw/8), y - cantonh/5)

    for stars in range(5):
        t.pendown()
        draw_star(t, stard)
        t.penup()
        t.forward(cantonw/12)
# ----------------------------------------------------------------------

# Part 4 - add code below for at least 3 tests cases for draw_US_flag()

def main():
    # Create a graphics window, a canvas to draw on, and a turtle
    graphics_window, my_canvas, sam = create_window()
    draw_usa_flag(sam, -100, 200, 200)
    # Keep the graphics window open until the user closes it.
    TK.mainloop()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
