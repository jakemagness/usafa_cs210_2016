#!/usr/bin/env python

"""
Lab07 - Learn how to use imported modules.

NOTE: A typical Python program will import all required external modules
      at the top of its file. For this lab exercise you will import the
      necessary modules for specific problems in the problem's solution.
      This is non-standard formatting and will not be done in future files.
"""
# =====================================================================

# Metadata
__author__ = "Jake Magness"
__email__ = "C18Jake.Magness@usafa.edu"
__date__ = "Aug 29, 2016"

# ----------------------------------------------------------------------
# Problem 1
# Import the math module and then use it's functionality to calculate
# and display the following values:
#   1) The value of pi
#   2) The square root of 42. (Use the sqrt function.)
#   3) The base 2 logarithm of 1000, 4096, and 4097.
#   4) The value of 0, 45, 90, 135, 180, 225, 270, 315, and 360 degrees
#      in radians. Use a loop to accomplish this; put the
#      required values in a list and iterate over the list.
#   5) Solve #4 again, but this time implement a loop controlled by a
#      range() function.
#   6) Display the sin and cos values for each of the values in #4. Note
#      that trig functions are always calculated using radians.
# ----------------------------------------------------------------------
# import math
#
# # 1
# print(math.pi)
#
# # 2
# print(math.sqrt(42))
#
# # 3
# for i in(1000, 4096, 4097):
#     print(math.log2(i))
# # 4
# print("")
# for i in (0, 45, 90, 135, 180, 225, 270, 315, 360):
#     print(math.radians(i))
#
# # 5
# print("")
# for i in range(9):
#     print(math.radians(45*(i)))
#
# # 6
# print("")
# for i in range(9):
#     print(math.cos(45*i))
#     print(math.sin(45*i))
# ----------------------------------------------------------------------
# Problem 2
# Implement the same code as problem 1, but this time do not import
# the entire math module. Only import the values and functions you need.
# Use the "from ModuleName import function1, function2, ..." format of
# the import command. NOTE: You will have to remove the module name from
# the call statements.
# ----------------------------------------------------------------------
# from math import pi, sqrt, log2, radians, cos, sin
#
# # 1
# print(pi)
#
# # 2
# print(sqrt(42))
#
# # 3
# for i in(1000, 4096, 4097):
#     print(log2(i))
# # 4
# print("")
# for i in (0, 45, 90, 135, 180, 225, 270, 315, 360):
#     print(radians(i))
#
# # 5
# print("")
# for i in range(9):
#     print(radians(45*(i)))
#
# # 6
# print("")
# for i in range(9):
#     print(cos(45*i))
#     print(sin(45*i))

# ----------------------------------------------------------------------
# Problem 3
# Implement the same code as problem 1, but this time do not import
# the entire math module. Only import the values and functions you use.
# And rename each function to make this horrible code.
# Use the "from ModuleName import function as newName" format of
# the import command. NOTE: You will have to change the function names
# in your statements accordingly.
# ----------------------------------------------------------------------
# from math import pi as F1
# from math import sqrt as F2
# from math import log2 as F3
# from math import radians as F4
# from math import cos as F5
# from math import sin as F6
#
# # 1
# print(F1)
#
# # 2
# print(F2(42))
#
# # 3
# for i in(1000, 4096, 4097):
#     print(F3(i))
# # 4
# print("")
# for i in (0, 45, 90, 135, 180, 225, 270, 315, 360):
#     print(F4(i))
#
# # 5
# print("")
# for i in range(9):
#     print(F4(45*(i)))
#
# # 6
# print("")
# for i in range(9):
#     print(F5(45*i))
#     print(F6(45*i))

# ----------------------------------------------------------------------
# Problem 4
# Import the random module and generate and print the following values
# using the functions in the random module. (Use for loops and the range
# function as needed.)
#    a) Print 10 random floating point values from the interval [0.0,1.0)
#    b) Print 10 random integer values from the interval [1,10]
#    c) Simulate the roll of two dice in a game and print 20 rolls.
# ----------------------------------------------------------------------
# import random
#
# # a
# for i in range(10):
#     number = float(random.random())
#     number = number*1
#     print(number)
#
# print("")
#
# # b
# for i in range(10):
#     n2 = random.randrange(1, 10)
#     print(n2)
#
# # c
# for i in range(20):
#     dice1 = random.randrange(1, 7)
#     dice2 = random.randrange(1, 7)
#     print("Dice 1 is: ")
#     print(dice1)
#     print("Dice 2 is: ")
#     print(dice2)
# ----------------------------------------------------------------------
# Challenge Problem 1
# Write a program that draws random sized rectangles of random colors
# at random locations on a canvas. Draw 100 or more rectangles.
# (Uncomment the code below as your starting point.)
# Note: You can create a random color by generating 3 random floating
#       point values in the range [0,1) and send the values to the
#       "color" function of a turtle as a [red, green, blue] value
# Note: You should now be able to understand the import statement that
#       is used below.
# ----------------------------------------------------------------------

# import random
# from turtle import TurtleScreen, RawTurtle, TK
#
# # Constants
# CANVAS_WIDTH = 600
# CANVAS_HEIGHT = int(CANVAS_WIDTH * 9 / 16)  # Produces the eye-pleasing 16:9 HD aspect ratio.
#
# # Create a Tkinter graphics window
# graphics_window = TK.Tk()
# graphics_window.title("Lab 7 Random Rectangles")
#
# # Create a "canvas" inside the graphics window to draw on
# my_canvas = TK.Canvas(graphics_window, width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
# my_canvas.pack()  # Organizes my_canvas inside the graphics window
#
# # Create a "canvas" made specifically for turtle graphics
# turtle_canvas = TurtleScreen(my_canvas)
# turtle_canvas.bgcolor("white")
#
# # Create a turtle to draw on the canvas
# fred = RawTurtle(turtle_canvas)
#
# # Make the turtle draw as fast as possible
# fred.hideturtle()
# fred.speed("fastest")
#
# # For loop that defines how many rectangles
# for i in range(30):
#     # Getting the random position
#     fred.penup()
#     # Variables that are the size of the canvas for turtle to spawn in
#     y = random.randrange(-100, 100)
#     x = random.randrange(-200,200)
#     fred.goto(x, y)
#     fred.pendown()
#
#     # Getting the random color
#     red = float(random.random())
#     red = red*1
#     blue = float(random.random())
#     blue = blue*1
#     green = float(random.random())
#     green = green*1
#     fred.pencolor(red, blue, green)
#
#     # Random size for squares
#     dist = random.randrange(1, 80)
#
#     # Draws the square
#     for i in range(4):
#         fred.forward(dist)
#         fred.left(90)
#
# # Keep the window open until the user closes it.
# TK.mainloop()

# ----------------------------------------------------------------------
# Challenge Problem 2
# Search the internet for a way to calculate an approximation for pi.
# There are many that use simple arithmetic. Write a program to compute
# the approximation and then print that value as well as the value of
# math.pi from the math module.
# ----------------------------------------------------------------------
from math import pi

den = 1
den2 = 3
p = 0

for i in range (300):
    p = (1/den) + p
    den = den + 4

for i in range (300):
    p = p - (1/den2)
    den2 = den2 + 4


p = 4*p
print (p)
print (pi)
