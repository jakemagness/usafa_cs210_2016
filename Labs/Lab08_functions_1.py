#!/usr/bin/env python

"""
Lab08 - Learn how to create and invoke functions.

NOTE: Since functions are not executed unless invoked, as you work
      through these problems, only comment out code that is executed and
      leave the functions un-commented. You will reuse functions written
      for one problem in other problems.
"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules

# Metadata
__author__ = "Jake"
__email__ = "Jake.Magness@usafa.edu"
__date__ = "Aug 31, 2016"

from turtle import TurtleScreen, RawTurtle, TK

# Constants
CANVAS_WIDTH = 600
CANVAS_HEIGHT = int(CANVAS_WIDTH * 9 / 16)  # Produces the eye-pleasing 16:9 HD aspect ratio.

# Create a Tkinter graphics window
graphics_window = TK.Tk()
graphics_window.title("Lab 8 - Functions")

# Create a "canvas" inside the graphics window to draw on
my_canvas = TK.Canvas(graphics_window, width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
my_canvas.pack()  # Organizes my_canvas inside the graphics window

# Create a "canvas" made specifically for turtle graphics
turtle_canvas = TurtleScreen(my_canvas)
turtle_canvas.bgcolor("white")

# Create a turtle to draw on the canvas
phil = RawTurtle(turtle_canvas)

# Make the turtle draw as fast as possible - comment these lines out for
# slower rendering.
# turtle_canvas.delay(0)
# phil.hideturtle()
# phil.speed("fastest")

# ----------------------------------------------------------------------
# Problem 1
# Use the drawsquare function we wrote in this chapter in a program to
# draw the example image for problem 1. Assume each side is 20 units.
# (Hint: notice that the turtle has already moved away from the ending
#  point of the last square when the program ends.)
# ----------------------------------------------------------------------

def draw_square(t, size):
    """
    Draw a square of a specified length
    :param t: Which turtle object is being called
    :param size: The defined size of one side of the square
    :return: None
    """
    for side in range(4):
        t.forward(size)
        t.left(90)


# for i in range(5):
#     phil.setheading(0)
#     phil.penup()
#     phil.forward(40)
#     phil.pendown()
#     draw_square(phil, 20)


# ----------------------------------------------------------------------
# Problem 2
# Write a program to draw the design in the image. Assume the innermost
# square is 20 units per side, and each successive square is 20 units
# bigger, per side, than the one inside it.
# ----------------------------------------------------------------------

# for n in range(6):
#     phil.pendown()
#     draw_square(phil, 20*n)
#     phil.penup()
#     phil.goto(-10*n, -10*n)



# ----------------------------------------------------------------------
# Problem 3
# Write a non-fruitful function drawPoly(a_turtle, num_sides, side_length)
# which makes a turtle draw a regular polygon. When called with
# drawPoly(tess, 8, 50), it draws a polygon with 8 sides, where each
# side has a length of 50.
# ----------------------------------------------------------------------

def polydraw(t, numsides, sidelen):
    for n in range(numsides):
        phil.forward(sidelen)
        phil.left(360 / numsides)

# polydraw(phil, 8, 50)
# ----------------------------------------------------------------------
# Problem 4
# Draw a pattern created by the superposition of squares rotated at
# successive 18 degree increments. Your solution must use a function
# to draw the square.
# ----------------------------------------------------------------------

# for i in range(20):
#     draw_square(phil,40)
#     phil.penup()
#     phil.goto(0, 0)
#     phil.pendown()
#     phil.setheading(18*i)

# ----------------------------------------------------------------------
# Problem 5
# Implement a function that draws a 4 sided spiral, where each side of
# the spiral grows longer by 2 units. The change in angle between each
# side of the spiral must be a parameter to the function. Then call the
# function twice to produce the demo images (using angles of 90 and 89).
# (Experiment with other angles besides the ones needed for the demo
# images -- if you have extra time.)
# ----------------------------------------------------------------------

# phil.setheading(270)
def spirial(t, angle):
    for i in range(80):
        phil.forward(i*2)
        phil.right(angle)
    phil.penup()
    phil.goto(0, 0)
    phil.pendown()

# spirial(phil, 90)
# spirial(phil, 89)
# ----------------------------------------------------------------------
# Problem 6
# Write a non-fruitful function drawEquitriangle(a_turtle, side_length)
# which calls drawPoly from the previous question to have its turtle
# draw a equilateral triangle. Make 2 or more calls to this new function.
# ----------------------------------------------------------------------

def drawEquitriangle(a_turtle, side_length):
    for i in range(3):
        a_turtle.forward(side_length)
        a_turtle.left(120)
    a_turtle.penup()
    a_turtle.forward(50)
    a_turtle.pendown()

# for i in range(2):
#     drawEquitriangle(phil, 30)

# ----------------------------------------------------------------------
# Problem 7
# Write a fruitful function sumTo(n) that returns the sum of all integer
# numbers up to and including n. Use the equation (n * (n + 1)) // 2 to
# calculate your answer. Then implement another function that calculates
# the same sum, but this time using the "accumulator pattern." Call this
# function sumUp(n). Call the functions several times to verify that
# they get the correct answer. Note: The sum 1+2+3...+10 is 55.
# ----------------------------------------------------------------------


def sumTo(n):
    ans = (n*(n+1)//2)
    print(ans)
def sumUp(n):
    ans = 0
    for i in range(n+1):
        ans = i + ans
    print(ans)

# sumTo(10)
# sumUp(10)
# ----------------------------------------------------------------------
# Problem 8
# Write a function areaOfCircle(radius) which returns the area of a
# circle. Use the math module in your solution appropriately. Call your
# function several times to verify its correctness.
# ----------------------------------------------------------------------
def areaOfCircle(r):
    from math import pi
    area = pi*r**2
    print(area)
# areaOfCircle(10)


# Keep the graphics window open until the user closes it.
TK.mainloop()

