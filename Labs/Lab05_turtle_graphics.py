#!/usr/bin/env python

"""
Lab05 - Learning turtle graphics.
"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules
import math
from turtle import TurtleScreen, RawTurtle, TK

# Metadata
__author__ = "Jake"
__email__ = "C18Jake.Magness@usafa.edu"
__date__ = "Aug 23, 2016"

# Constants
CANVAS_WIDTH = 600
CANVAS_HEIGHT = int(CANVAS_WIDTH * 9 / 16)  # Produces the eye-pleasing 16:9 HD aspect ratio.

# Create a Tkinter graphics window
graphics_window = TK.Tk()
graphics_window.title("Lab 5 Problems")

# Create a "canvas" inside the graphics window to draw on
my_canvas = TK.Canvas(graphics_window, width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
my_canvas.pack()  # Organizes my_canvas inside the graphics window

# Create a "canvas" made specifically for turtle graphics
turtle_canvas = TurtleScreen(my_canvas)
turtle_canvas.bgcolor("white")

# Create a turtle to draw on the canvas
mary = RawTurtle(turtle_canvas)

#----------------------------------------------------------------------
# Problems 2
# Turtle objects have methods and attributes. For example, a turtle has
# a position and when you move the turtle forward, the position changes.
# Think about the other methods shown in the summary above
# (http://interactivepython.org/runestone/static/USAFA_CS210_2016/PythonTurtle
# /SummaryofTurtleMethods.html). Which attributes, if any, does each
# method relate to? Does the method change the attribute?
#----------------------------------------------------------------------

# Answer:

# Turtle has no parameters and the method creates and returns a new turtle object on the screen
# forward has a distance parameter.  The turtle that calls the method will move the specified distance forward
# backward has a distance parameter.  The turtle that calls this method will move the specified distance backwards.
# right has an angle parameter.  The turtle that calls the right method will turn right the specified angle.
# left has an angle parameter.  The turtle that calls the left method will turn left the specified angle.
# up picks up the turtle's tail and has no parameter.  This is what makes the turtle draw lines, toggling the tail up
# will stop the drawing.
# sown puts the turtle's tail down. When the tail is toggled down, the turtle will draw a line behind it.
# color has the parameter color name.  When a turtle object calls the color method, it will change the color of the
# turtle's tail to the specified color.
# fillcolor has the method color name.  This specifies what color the turtle will use to fill a polygon.
# Heading has no parameter.  When the turtle object calls this method, the turtle will return its current heading.
# position has no parameter. When the turtle object calls this method, the turtle will return its position.
# goto has the x and y coordinates as parameters.  The turtle will move to the specific location when the method is
# invoked.
# begin_fill has no parameter and when is called, will remember the location as the starting point for a filled polygon
# end_fill has no parameter and whne called will denote the closure of the polygon and fill with current color
# dot has no parameter and leaves a dot at the current position
# stamp has no parameter and leaves an outline of the turtle at the current location
# shape has the parameter shapename.  There are four specified shape names that can change the shape of the turtle.

# #----------------------------------------------------------------------
# Problems 3
# Write a program that uses a for loop to print
#   One of the months of the year is January
#   One of the months of the year is February
#   One of the months of the year is March
#   etc ...
#----------------------------------------------------------------------
# for month in ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
#               "November", "December"]:
#     print("One of the months of the year is ", month)
#----------------------------------------------------------------------
# Problems 4
# Assume you have a list of numbers 12, 10, 32, 3, 66, 17, 42, 99, 20
#
#    Write a loop that prints each of the numbers on a new line.
#    Write a loop that prints each number and its square on a new line.
#----------------------------------------------------------------------
# for number in [12, 10, 32, 3, 66, 17, 42, 99, 20]:
#     print(number)
# for number in [12, 10, 32, 3, 66, 17, 42, 99, 20]:
#     print(number**2)
#----------------------------------------------------------------------
# Problems 5
# Use for loops to make a turtle draw these regular polygons (regular
# means all sides the same lengths, all angles the same):
#
#   An equilateral triangle
#   A square
#   A hexagon (six sides)
#   An octagon (eight sides)
#----------------------------------------------------------------------
# d = 30
# # Triangle
# for i in [120, 120, 120]:
#     mary.forward(30)
#     mary.left(i)
#
# # Square
# mary.forward(50)
#
# for i in [90, 90, 90, 90]:
#     mary.forward(d)
#     mary.left(i)
#
# # Hexagon
# mary.forward(50)
#
# for i in [60, 60, 60, 60, 60, 60]:
#     mary.forward(d)
#     mary.left(i)
#
# # Octagon
# mary.forward(70)
#
# for i in [45, 45, 45, 45, 45, 45, 45, 45]:
#     mary.forward(d)
#     mary.left(i)

#----------------------------------------------------------------------
# Problems 7
# A drunk pirate makes a random turn and then takes 100 steps forward,
# makes another random turn, takes another 100 steps, turns another
# random amount, etc. A social science student records the angle of
# each turn before the next 100 steps are taken. Her experimental data
# is 160, -43, 270, -97, -43, 200, -940, 17, -86.(Positive angles are
# counter-clockwise.) Use a turtle to draw the path taken by our
# drunk friend. After the pirate is done walking, print the current
# heading.
#----------------------------------------------------------------------
# d = 100
#
# for i in [160, -43, 270, -97, -43, 200, -940, 17, -86]:
#     mary.left(i)
#     mary.forward(d)
#----------------------------------------------------------------------
# Problems 8
# On a piece of scratch paper, trace the following program and show the
# drawing. When you are done, un-comment the code and execute it to check
# your answer.
#----------------------------------------------------------------------

# tess = RawTurtle(turtle_canvas)
# tess.right(90)
# tess.left(3600)
# tess.right(-90)
# tess.left(3600)
# tess.left(3645)
# tess.forward(-100)
#----------------------------------------------------------------------
# Challenge Problems 1
# Implement a loop within a loop that draws the pattern shown in the Lab05
# description. It is created by drawing a set of square where each square
# is orientated 30 degrees from the previous square.
#----------------------------------------------------------------------
# mary.up()
# mary.goto(-10,-10)
# mary.down()
# for i in range(2):
#     for x in range(4):
#         mary.forward(20)
#         mary.left(90)
#
#     mary.up()
#     mary.goto(0,0)
#     mary.down()
#     mary.left(30)
#
# h = mary.heading()
# g = mary.position()
#
# print(h)
# print(g)




# for i in range(12):
#     mary.forward(30)
#     mary.left(180)
#     mary.forward(30)
#     mary.left(30)

#----------------------------------------------------------------------
# Challenge Problem 2
# Implement a program to draw the pattern shown in the Lab05 description.
#----------------------------------------------------------------------


# Keep the window open until the user closes it.
TK.mainloop()