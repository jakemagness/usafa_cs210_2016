#!/usr/bin/env python

"""
Lab06 - Learn more about turtle graphics and problem solving.
"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules
import math
from turtle import TurtleScreen, RawTurtle, TK

# Metadata
__author__ = "Jake Magness"
__email__ = "Jake.Magness@usafa.edu"
__date__ = "Aug 25, 2016"

# Constants
CANVAS_WIDTH = 600
CANVAS_HEIGHT = int(CANVAS_WIDTH * 9 / 16)  # Produces the eye-pleasing 16:9 HD aspect ratio.

# Create a Tkinter graphics window
graphics_window = TK.Tk()
graphics_window.title("Lab 5 Problems")

# Create a "canvas" inside the graphics window to draw on
my_canvas = TK.Canvas(graphics_window, width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
my_canvas.pack()  # Organizes my_canvas inside the graphics window

# Create a "canvas" made specifically for turtle graphics
turtle_canvas = TurtleScreen(my_canvas)
turtle_canvas.bgcolor("white")

# Create a turtle to draw on the canvas
mary = RawTurtle(turtle_canvas)

#----------------------------------------------------------------------
# Problems 1
# Write a program that prints "We like Python's turtles!" 1000 times.
#----------------------------------------------------------------------
# for i in range(1000):
#     print("We like Python's turtles!")

# ----------------------------------------------------------------------
# Problems 6
# Write a program that asks the user for the number of sides, the length
# of the side, the color, and the fill color of a regular polygon. The
# program should draw the polygon and then fill it in.
# ----------------------------------------------------------------------
# Get user input and change the variable type to desired type
# sides = input("Enter the number of sides: ")
# sides = int(sides)
# color = input("Enter the color you want: ")
#
# # Change turtle color to input defined color as well as begin to fill the polygon
# mary.color(color)
# mary.fillcolor(color)
# mary.begin_fill()
#
# # Loop to create polygon
# for i in range(sides):
#     mary.forward(30)
#     mary.left(360/sides)
#
# # End polygon fill to fill in shape
# mary.end_fill()

#----------------------------------------------------------------------
# Problems 9
# Write a program to draw a 5 sided star. See the problem in the textbook
# for an example figure. Hint: The angle formed by each point of the star
# is 36 degrees. (It is also possible to draw a 10 pointed star with an
# angle of 72 degrees at each point.
#----------------------------------------------------------------------

for i in range(6):
    mary.forward(60)
    mary.right(144)

#----------------------------------------------------------------------
# Problems 10
# Draw an image of a clock face, where each "number" on the clock face
# is a stamp of a turtle. Refer to problem 10 in the textbook for an
# example image.
#----------------------------------------------------------------------
# # Changes shape to turtle and makes the color blue
# mary.shape("turtle")
# mary.color("blue")
#
# # Stamps turtle in the center and lifts up pen to not draw out to clock edge
# mary.stamp()
# mary.penup()
#
# for i in range(12):
#     # Moves turtle forward
#     mary.forward(50)
#
#     # Puts pen down to draw line
#     mary.pendown()
#     mary.forward(15)
#
#     # Pen up to stop drawing to stamp location
#     mary.penup()
#     mary.forward(15)
#
#     # Stamp, return to center, and then rotate
#     mary.stamp()
#     mary.goto(0,0)
#     mary.right(30)

#----------------------------------------------------------------------
# Problems 11
# Write a program to draw some kind of picture. Be creative and experiment
# with the turtle methods provided in Summary of Turtle Methods.
#----------------------------------------------------------------------

# # Get user input and change the variable type to desired type
#
# sides = 5
# color = "blue"
#
# # Change turtle color to input defined color as well as begin to fill the polygon
# mary.color(color)
# mary.fillcolor(color)
# mary.begin_fill()
#
#  # Loop to create polygon
# for i in range(sides):
#      mary.forward(30)
#      mary.left(360/sides)
#
# # End polygon fill to fill in shape
# mary.end_fill()
#
# mary.penup()
# mary.goto(-10,29)
# mary.color("white")
# mary.pendown()
#
# # Make inside
# for n in range(6):
#      mary.forward(50)
#      mary.right(144)

#----------------------------------------------------------------------
# Problems 12
# Create a turtle and assign it to a variable. When you print its type,
# what do you get?
#----------------------------------------------------------------------

# When printing an object shows the location in hex at which the object is stored
# print(mary)

#----------------------------------------------------------------------
# Problems 13
# A sprite is a simple spider shaped thing with n legs coming out from
# a center point. The angle between each leg is 360 / n degrees. Write
# a program to draw a sprite where the number of legs is provided by the
# user.
#----------------------------------------------------------------------
# Ask user for the amount of legs
# legs = input("How many legs for the sprite? ")
# legs = int(legs)
#
# # Create an angle for the turn, need to divide by 2 or legs overlap
# turn = 360/legs/2
#
# # Loop continues until all legs are drawn
# for i in range(legs):
#     # Turn angle for legs
#     mary.right(turn)
#     mary.forward(30)
#
#     # Retrace steps back to center
#     mary.right(180)
#     mary.forward(30)
#
#     # Orient so that turtle draws in new direction
#     mary.left(180-turn)

#----------------------------------------------------------------------
# Challenge Problem 1
# Copy and paste your code from problem 9 that draws a 5-point star and
# modify it to draw a star of any size based on a variable called "size".
# Then modify it to draw the star centered at the current location.
# Now draw multiple stars in multiple locations of different sizes on
# the canvas.
#----------------------------------------------------------------------

size = 60
mary.penup()
mary.goto(-(size/2),10)
mary.pendown()

for i in range(6):
    mary.forward(size)
    mary.right(144)

mary.penup()
mary.goto(0,0)

# Create new stars

mary.goto(-90,0)
mary.pendown()
size = 40

for i in range(6):
    mary.forward(size)
    mary.right(144)

mary.penup()
mary.goto(-120, 50)
mary.pendown()
size = 80

for i in range(6):
    mary.forward(size)
    mary.right(144)


# Keep the window open until the user closes it.
TK.mainloop()