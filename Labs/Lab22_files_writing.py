#!/usr/bin/env python

"""
Lab21 - Learn how to write text files.
"""
# =====================================================================

# Metadata
__author__ = "Jake Magness"

# ---------------------------------------------------------------------
# Problem 1
# Please make sure you have downloaded the data files from the Moodle server
# before starting this lab.
#
# Assume that you have opened a file for reading with this command:
#
#       file_object = open("example.txt", "r")
#
# Explain what each of the following methods on the file object would do.
# Describe the data type of the variable "data" and what it will contain.
# After you have described each call, discuss your answers with a fellow cadet.
#
#   data = file_object.read()
#   - data is ...
#
#   data = file_object.readlines()
#   - data is ...
#
#   data = file_object.readline()
#   - data is ...
#
#   for data in file_object:
#   - data is ...
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
# Problem 2
# Write a function that reads the studentdata.txt file and creates a new
# text file that contains one line of output for each student in the file.
# Each line should contain the student's name and their average grade on
# all assignments. Note that some students have more grades than others.
#
# Make your output file look like this:
'''
joe        23.00
bill       20.00
sue        16.17
grace      23.67
john       35.20
'''
# ----------------------------------------------------------------------

def problem2():


    infile = open("../Data/studentdata.txt", "r")
    outfile = open("../Data/studentfile.txt", "w")
    average = 0
    aline = infile.readline()

    while aline:
        items = aline.split()

        for i in range(len(items)-1):
            num = float(items[i+1])
            average = (average + num)

        # dataline = items + ',' + items[0]
        value = average/(len(items)-1)

        outfile.write("{} {} {:.2f} {}".format(items[0], "\t", value, "\n"))
        aline = infile.readline()
        average = 0

    infile.close()
    outfile.close()


# ----------------------------------------------------------------------
# Problem 3
# The code in the multiplication_table() function below prints a
# multiplication table. Modify the code so that it saves the table to a
# file. Add a parameter to the function that specifies the file name
# of the output file.
# ----------------------------------------------------------------------


def multiplication_table(rows, columns):
    outfile = open("../Data/multable.txt", "w")

    # Print the top heading
    print("  * |", end='')
    for c in range(columns):
        print("{:5d}".format(c), end='')
        outfile.write("{}".format("{:5d}".format(c)))
    print()

    # Print a dividing line between the heading and the table.
    print("    |", end='')
    outfile.write("{} {}".format("    |", "\n"))
    print("-----" * columns)
    outfile.write("{} {}".format("-----" * (columns + 2), "\n"))

    # Print the rows of the table
    for r in range(rows):
        print("{:3d} |".format(r), end='')
        outfile.write("{}".format("{:3d} |".format(r)))
        for c in range(columns):
            print("{:5d}".format(r * c), end='')
            outfile.write("{}".format("{:5d}".format(r * c)))
        outfile.write("\n")
        print()

    outfile.close()
# ----------------------------------------------------------------------
# Problem 4
# Write a function that prints "I love Python" to an output file 1 million
# times.
#
# After you have a working program, speculate on how hard it would be to
# write a malicious program that filled up a person's hard drive.
#  ----------------------------------------------------------------------


def problem4():
    pass

# ----------------------------------------------------------------------
# Problem 5
# If you have not read the PEX 3 assignment, read it now. Then write a
# function that will save an instance of a RACK-O game (as defined in the
# PEX 3 description) to a text file. A "RACK-O instance" is defined in the
# main() function below for your testing.
#  ----------------------------------------------------------------------


def save_game(game):
    pass

# ----------------------------------------------------------------------

# Note: File processing is really string processing because the contents
#       of a text file is always a string.


def main():
    print("Call your functions to verify their correctness.")

    # problem2()
    multiplication_table(10, 10)
    # problem4()

    # game = [0,  # DEALER
    #         1,  # ACTIVE_PLAYER
    #         [[1, 2, 3, 4, 5, 6, 7, 8, 9, 10],  # HANDS
    #          [11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
    #          [21, 22, 23, 24, 25, 26, 27, 28, 29, 30]],
    #         [31, 32, 33, 34, 35, 36, 37, 38, 39, 40,  # DRAW_PILE
    #          41, 42, 43, 44, 45, 46, 47, 48, 49, 50],
    #         [],  # DISCARD_PILE
    #         [0, 0, 0]  # PLAYER_SCORES
    #        ]
    #
    # save_game(game)

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
