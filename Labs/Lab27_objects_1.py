#!/usr/bin/env python

"""
Lab27 - Learn how to create and use objects.
"""
# =====================================================================

import math

# Metadata
__author__ = "Jake Magness"

# ---------------------------------------------------------------------


class Point:
    """ Point class for representing and manipulating x,y coordinates. """

    def __init__(self, init_x, init_y):
        """ Create a new point at the given coordinates. """
        self.x = init_x
        self.y = init_y

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def distance_from_origin(self):
        return ((self.x ** 2) + (self.y ** 2)) ** 0.5

    def set_x(self, x):
        self.x = x

    def set_y(self, y):
        self.y = y

    def is_at_orgin(self):
        if self.get_x() == 0:
            if self.get_y() == 0:
                return True
        else:
            return False

    def move(self, x, y):
        self.y = self.y + y
        self.x = self.x + x

class Circle:

    def __init__(self, init_radius, init_x, init_y):
        """ Create a new point at the given coordinates. """
        self.init_x = init_x
        self.init_y = init_y
        self.radius = init_radius

    def circumference(self):
        return 2*3.14*self.radius

    def expand_circle(self, new_radius):
        self.radius = new_radius

    def get_radius(self):
        return self.radius
# ---------------------------------------------------------------------

# =====================================================================
# PLEASE NOTE:
# The structure of this lab assignment is different from previous labs.
# The problems you are assigned to implement are described below, but you
# will put your "answers" either in the Point class above, or the main()
# function below.
# =====================================================================
# Problem 1:
# Create at least 3 more instances of the Point class in the main() function.
# Give each Point a different (x,y) location. Print out each Point's
# location to verify that each point has its own (x,y) values.
# ---------------------------------------------------------------------
# Problem 2:
# Add a method to the Point class to change the value of the x component.
# Name the method "set_x".
# Call your method from the main() function to verify its correctness.
# ---------------------------------------------------------------------
# Problem 3:
# Add a method to the Point class to change the value of the y component.
# Name the method "set_y".
# Call your method from the main() function to verify its correctness.
# ---------------------------------------------------------------------
# Problem 4:
# Add a method to the Point class that returns True if the point is
# at the origin, and false otherwise. (I.e., is the point (0,0)?)
# Name your method "is_at_origin".
# Call your method from the main() function to verify its correctness.
# ---------------------------------------------------------------------
# Problem 5:
# Add a method to the Point class that moves the point a specified
# distance along the x and y axis. The function should receive 2
# parameters: how much to change x and how much to change y.
# Name you method "move".
# Call your method from the main() function to verify its correctness.
# ---------------------------------------------------------------------
# Problem 6:
# Define a new class that represents a circle object. Your class should
# represent a circle using a center point and a radius. Add at least
# 3 useful methods to the circle class.
# Name your class "Circle". (Always use the standard convention that class
# names start with a capital letter.)
# Include a docstring that described your class.
# In main(), create multiple instances of your Circle class and call its
# methods.
# =====================================================================


def main():
    """
    Use a Point class to create Point objects and manipulate the objects.
    """

    # p = Point(7, 6)
    # print("p = ({},{})".format(p.get_x(), p.get_y()))
    #
    # q = Point(3, 5)
    # print(q.distance_from_origin())
    #
    # a = Point(1, 4)
    # print("a = ({},{})".format(a.get_x(), a.get_y()))
    # b = Point(2, 4)
    # print("b = ({},{})".format(b.get_x(), b.get_y()))
    # c = Point(6, 7)
    # print("c = ({},{})".format(c.get_x(), c.get_y()))
    #
    # c.set_x(2)
    # print(c.get_x())
    #
    # c.set_y(4)
    # print(c.get_y())
    #
    # print(c.is_at_orgin())
    # c.set_x(0)
    # c.set_y(0)
    # print(c.is_at_orgin())
    #
    # c.move(2, 4)
    # print(c.get_x())
    # print(c.get_y())

    one = Circle(2, 4, 4)
    two = Circle(1, 5, 2)
    three = Circle(4, 7, 1)

    print(one.circumference())

    two.expand_circle(8)
    print(two.get_radius())
# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
