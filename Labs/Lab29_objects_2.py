#!/usr/bin/env python

"""
Lab29 - Learn more about how to create and use objects.
"""
# =====================================================================

import math

# Metadata
__author__ = "First Last"

# ---------------------------------------------------------------------


class Point:
    """ Point class for representing and manipulating x,y coordinates. """

    def __init__(self, init_x, init_y, color):
        """ Create a new point at the given coordinates. """
        self.x = init_x
        self.y = init_y
        self.color = color

    def __str__(self):
        string = "X is: {} {}The color is: {}".format(self.get_x(), "\n", self.color, "\n")
        return string

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def distance_from_origin(self):
        return ((self.x ** 2) + (self.y ** 2)) ** 0.5

    def set_x(self, new_x):
        self.x = new_x

    def set_y(self, new_y):
        self.y = new_y

    def is_at_origin(self):
        return self.x == 0 and self.y == 0

    def move(self, delta_x, delta_y):
        self.x += delta_x
        self.y += delta_y

    def change_color(self, new_color):
        self.color = new_color

    def midpoint(self, point1):
        point0x = self.get_x()
        point0y = self.get_y()
        point1x = point1.get_x()
        point1y = point1.get_y()


        midpointx = (point0x + point1x)/2
        midpointy = (point0y + point1y)/2

        new_point = Point(midpointx, midpointy, "Blue")
        return new_point
# ---------------------------------------------------------------------


class Circle:
    """ Define a circle. """

    def __init__(self, point, radius, color):
        self.center = point
        self.radius = radius
        self.color = color

    def __str__(self):
        string = "The radius is: {} {}The color is: {} {}".format(self.radius, "\n", self.color, "\n")
        return string

    def radius(self):
        return self.radius

    def area(self):
        return math.pi * self.radius ** 2

    def move(self, delta_x, delta_y):
        self.center.move(delta_x, delta_y)

    def get_circumference(self):
        return 2*3.14*self.radius

    def copy(self):
        new_radius = self.radius
        new_point = self.center
        new_color = self.color
        new_circle = Circle(new_radius, new_point, new_color)
        return new_circle

# =====================================================================
# PLEASE NOTE:
# The structure of this lab assignment is different from previous labs.
# The problems you are assigned to implement are described below, but you
# will put your "answers" either in the class definitions above, or the
# main() function below.
# =====================================================================
# Problem 1:
# Study the above class definitions for a Point and Circle class. (These
# are example solutions to the lab 27 problems.) Discuss the solutions
# with a fellow cadet. If you have any questions, please ask your
# instructor for assistance.
#
#  ---------------------------------------------------------------------
# Problem 2:
# Discussion:
# Python "classes" have special methods for common tasks.
#   * The special methods always start and end with two underscores (__).
#   * When a class is defined, Python always adds its special methods to
#     the definition. You can't change this! This is just how Python
#     works!
#   * One method that is always added to a class definition is the
#     __str__(self) method that converts the object to a string. The
#     default __str__() method is typically not very useful.
#   * You can override the default __str__() method by defining your
#     own __str__() method in your class. In this way you can always
#     determine exactly how an object is converted to a string.
#
# Task:
# Implement a __str(self) function for both the Point and Circle classes.
# Note that a __str__(self) method always returns a string.
# Test your work by creating objects (instances of a class) in main()
# and then printing out the objects.
#
# ---------------------------------------------------------------------
# Problem 3:
# Modify the __str(self) methods you implemented for problem 2 to make the
# objects print out differently. Be creative!
#
# Test your work by creating objects (instances of a class) in main()
# and then printing out the objects.
#
#  ---------------------------------------------------------------------
# Problem 4:
# Add a color value to both the Point and Circle class' internal data.
# That is, a Point should be defined by 3 values: x, y, color.
# A Circle should be defined by 3 values: a point, a radius, and a color.
# Modify any methods of the classes that might be effected by this change.
#
# Note: If you give a parameter a default value, you are not required
#       to send a value when you call the function.
#
# Test your work by creating objects (instances of a class) in main()
# and then calling appropriate methods on the objects.
#
#  ---------------------------------------------------------------------
# Problem 5:
# Add at least one new method to both class definitions. Here are a few
# possible suggestions:
# For the Point class: get_color, set_color, which_quadrant, angle,
#                      distance_from(another_point)
# For the Circle class: circumference, shrink(percent), point_inside(a_point)
#
# Test your work by creating objects (instances of a class) in main()
# and then calling appropriate methods on the objects.
#
#  ---------------------------------------------------------------------
# Problem 6:
# The purpose of this problem is to implement a class method that returns
# an object as its return value, which is a common occurrence. For example,
# all string methods return new objects because strings are immutable.
#
# Implement a new method for the Point class that calculates the midpoint
# between itself and another point and returns the midpoint as a new
# Point object. The method definition should look like this:
#     def mid_point(self, another_point):
#       ...
#       return mid_point
#
# Implement a new method for the Circle class that creates an exact copy
# of a Circle object without changing the original in any way. The
# method definition should look like this:
#     def copy(self):
#       ...
#       return new_circle
#
# Test your work by creating objects (instances of a class) in main()
# and then calling appropriate methods on the objects.
#
# ---------------------------------------------------------------------

# =====================================================================


def main():
    """
    Use a Point and Circle class to create Point and Circle objects
    and then manipulate those objects.
    """

    p = Point(7, 6, "Blue")
    print("p =", p)

    q = Point(3, 5, "Blue")
    print("q =", q)

    r = Point(7, 6, "Green")
    print("r =", r)

    s = Circle(r, 5, "Red")
    print("s =", s)

    t = Circle(5, 10, "Yellow")
    print(t)

    p.change_color("RED")
    print(t.get_circumference())

    point0 = Point(4, 2, "Blue")
    point1 = Point(2, 4, "Yellow")
    answer = point0.midpoint(point1)
    print(answer.get_x())
    print(answer.get_y())



    circle = Circle(2, 4, "Blue")
    circle.copy()
    print(circle)
# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
