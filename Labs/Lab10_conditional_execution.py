#!/usr/bin/env python

"""
Lab10 - Learn how to execute some statements while skipping other
        statements -- that is, how to implement conditional execution.

        All of the problems in this lab exercise are written as function
        to give you additional practice in writing functions. As in previous
        labs, comment out your answers when you move on to the next problem,
        but you do not need to comment out the functions.
"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules

# # Metadata
__author__ = "Jake Magness"
__email__ = "Jake.Magness@usafa.edu"
__date__ = "Sept 8, 2016"

# ----------------------------------------------------------------------
# Problem 1
# What do these expressions evaluate to?
#   3 == 3
#   3 != 3
#   3 >= 4
#   not (3 < 4)
#
# Print out the expressions to verify your answers.
# ----------------------------------------------------------------------
# print(3==3)
# print(3!=3)
# print(3>=4)
# print(not(3<4))
# ----------------------------------------------------------------------
# Problem 2
# Give the logical opposites of these conditions. You are not allowed to
# use the not operator.
#   a > b
#   a >= b
#   a >= 18  and  day == 3
#   a >= 18  or  day != 3
# Hint: For the last two expressions, do a google search for De Morgan's law
# ----------------------------------------------------------------------

# print("a < b")
# print("a <= b")
# print("a <= 18 or day != 3")
# print("a <= 18 and day = 3")

# ----------------------------------------------------------------------
# Problem 3
# Write a function which is given an exam mark, and it returns a
# string — the grade for that mark — according to this scheme:
#
#  Mark	     Grade
#  >= 90	   A
#  [80-90)	   B
#  [70-80)	   C
#  [60-70)	   D
#  < 60	       F
# The square and round brackets denote closed and open intervals.
# A closed interval includes the number, and open interval excludes
# it. So 79.99999 gets grade C , but 80 gets grade B.
#
# Test your function by printing the mark and the letter grade for a
# number of different marks.
# ----------------------------------------------------------------------

def lettergrade (grade):
    if grade >= 90:
        letter = "A"
    elif grade >= 80:
        letter = "B"
    elif grade >= 70:
        letter = "C"
    elif grade >= 60:
        letter = "D"
    else:
        letter = "F"
    print(letter)


# ----------------------------------------------------------------------
# Problem 4
# Modify the turtle bar chart program from the previous chapter so
# that the bar for any value of 200 or more is filled with red, values
# between [100 and 200) are filled yellow, and bars representing values
# less than 100 are filled green.
#
# The code from the textbook is provided below as a starting point.
# Un-comment the test code and the bar-chart will draw correctly. Then
# modify the function code to implement the different colored bars.
# ----------------------------------------------------------------------
import turtle


def draw_bar(t, height):
    """
    Draw one vertical bar of a bar chart.
    :param t: a turtle
    :param height: the height of the bar to draw
    :return: None
    """
    t.begin_fill()
    if height >= 200:
        t.fillcolor("red")
    elif height >= 100:
        t.fillcolor("yellow")
    else:
        t.fillcolor("green")

    t.left(90)
    t.forward(height)
    if height > 0:
        t.write(str(height))  # Label the height of this bar
    t.right(90)
    t.forward(40)
    t.right(90)
    t.forward(height)
    if height < 0:
        t.penup()
        t.right(90)
        t.forward(20)
        t.write(str(height))
        t.left(180)
        t.forward(20)
        t.right(90)
        t.pendown()
    t.left(90)

    t.end_fill()


def draw_bar_chart(bar_heights):
    """
    Draw a bar chart inside a graphics window
    :param bar_heights: a list of bar heights
    :return: None
    """
    # Calculate the size of the window needed to draw the bar chart
    maxheight = max(bar_heights)
    minheight = min(0, min(bar_heights))
    numbars = len(bar_heights)
    border = 10  # the width of a border on the left, right, and top sides

    # Create a window to draw the bar chart and scale it based on the
    # number of bars in the chart and the maximum height of any bar.
    wn = turtle.Screen()
    wn.setworldcoordinates(0 - border, minheight - border,
                           40 * numbars + border,  # width
                           maxheight + (-minheight) + 2 * border)  # height
    wn.bgcolor("lightgreen")

    # Create a turtle to draw the bar chart
    tess = turtle.Turtle()  # create tess and set some attributes
    tess.color("blue")
    tess.pensize(3)

    # Draw the bar chart
    for a_bar in bar_heights:
        draw_bar(tess, a_bar)

    # Keep the graphics window open until a user clicks in it.
    wn.exitonclick()

# Test the draw_bar_chart() function
# bar_heights = [48, 117, 200, 240, 160, 260, 220]
# draw_bar_chart(bar_heights)

# ----------------------------------------------------------------------
# Problem 5
# In the turtle bar chart program, what do you expect to happen if one
# or more of the data values in the list is negative? Go back and try
# it out. Change the program so that when it prints the text value
# for the negative bars, it puts the text above the base of the bar
# (on the 0 axis).
#
# Modify the draw_bar() function above in problem 4 to accomplish this.
# Below is a test case to test you modifications.
# ----------------------------------------------------------------------

# bar_heights = [48, -100, 200, -50, 160, 260, 220]
# draw_bar_chart(bar_heights)

# ----------------------------------------------------------------------
# Problem 6
# Write a function called "hypotenuse" that receives the length of two sides
# of a right-angled triangle and returns the length of the hypotenuse.
# (Hint: x ** 0.5 will return the square root, or use sqrt from the math
# module.)
#
# Call your functions several times with different inputs to verify that
# the function is calculating the correct result.
# ----------------------------------------------------------------------
def hypotenuse(length):
    hyp = ((length**2)+ (length**2))**0.5
    print(hyp)

# ----------------------------------------------------------------------
# Problem 7
# Write a function called is_even(n) that takes an integer as an argument
# and returns True if the argument is an even number and False if it is odd.
#
# Call your function several times to verify its correctness.
# ----------------------------------------------------------------------
def is_even(n):
    if (n%2 != 0):
        print(False)
    else:
        print(True)
# is_even(57)
# ----------------------------------------------------------------------
# Problem 8
# Now write the function is_odd(n) that returns True when n is odd and
# False otherwise. Use the function is_even(n) to calculate your answer.
#
# Call your function several times to verify its correctness.
# ----------------------------------------------------------------------
def is_odd(n):
    if (n%2 != 0):
        print(True)
    else:
        print(False)
# is_odd(59)

# ----------------------------------------------------------------------
# Problem 9
# Write a function called "is_rightangled" which, given the length of
# three sides of a triangle, will determine whether the triangle is
# right-angled. Assume that the third argument to the function is
# always the longest side. It will return True if the triangle is
# right-angled, or False otherwise.
#
# Hint: floating point arithmetic is not always exactly accurate,
# so it is not safe to test floating point numbers for equality.
# If a good programmer wants to know whether x is equal or close
# enough to y, they would probably code it up as
#
#   if  abs(x - y) < 0.001:      # if x is approximately equal to y
#       ...
#
# Call your function several times to verify its correctness. Note that
# there is a built-in function called abs().
# ----------------------------------------------------------------------
import math

def is_rightangled(l1, l2, l3):
    hyp = ((l1 ** 2) + (l2 ** 2)) ** 0.5
    if abs(hyp) == l3:
        print(True)
    else:
        print(False)
# is_rightangled(3, 4, 5)
# ----------------------------------------------------------------------
# Problem 10
# Write a new version of the "is_rightangled" function above so that
# the sides can be given to the function in any order.
#
# Call your function several times to verify its correctness.
# ----------------------------------------------------------------------
def is_rightangled2(l1, l2, l3):
    if (l1 > l2):
        if (l1 > l3):
            small1 = l2
            small2 = l3
            largest = l1
    elif (l2 > l3):
        small1 = l1
        small2 = l3
        largest = l2
    elif (l3 > l1):
        small1 = l1
        small2 = l2
        largest = l3
    else:
        largest = l1

    hyp = ((small1 ** 2) + (small2 ** 2)) ** 0.5


    if abs(hyp) == largest:
        print(True)
    else:
        print(False)
# is_rightangled2(5, 4, 3)
# Code has isssues if the same values are added, which is just more if statements.  Ran out of time
# ----------------------------------------------------------------------
# Challenge Problem 1
# A year is a leap year if it is divisible by 4 unless it is a century
# that is not divisible by 400. Write a function that takes a year as
# a parameter and returns True if the year is a leap year, False otherwise.
# Note: the years 1700, 1800, and 1900 are not leap years, but the years
# 1600 and 2000 are leap years.
#
# Call your function several times to verify its correctness.
# ----------------------------------------------------------------------

def leap_year (year):
    leap = year/4
    if (year%4 == 0):
        if (leap%4 == 0):
            print(True)
        else:
            print(False)
    else:
        print(False)

# leap_year(2015)
# ----------------------------------------------------------------------
# Challenge Problem 2
# Implement a calculator for the date of Easter.
# The following algorithm computes the date for Easter Sunday for any
# year between 1900 to 2099.
#
# Ask the user to enter a year. Compute the following:
#
#   a = year % 19
#   b = year % 4
#   c = year % 7
#   d = (19 * a + 24) % 30
#   e = (2 * b + 4 * c + 6 * d + 5) % 7
#   dateofeaster = 22 + d + e
#
# Special note: The algorithm can give a date in April. March has 31 days
# Also, if the year is one of four special years (1954, 1981, 2049, or 2076)
# then subtract 7 from the date.
#
# Your program should print an error message if the user provides a
# date that is out of range.
#
# Call your function several times to verify its correctness.
# ----------------------------------------------------------------------
