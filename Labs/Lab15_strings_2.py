#!/usr/bin/env python

"""
Lab15 - Learn more string manipulation.
"""
# =====================================================================

# # Metadata
__author__ = "Jake Magness"

# ----------------------------------------------------------------------
# Problem 1
# Write a function that removes all occurrences of a given letter from
# a string. It's input parameters should be a string and the letter to
# remove.
# ----------------------------------------------------------------------

def remove(word, letter):

    new = ""

    for i in range(len(word)):
        if word[i] == letter:
            new = new
        else:
            new = new + word[i]


# ----------------------------------------------------------------------
# Problem 2
# Write a function that counts how many times a substring occurs in a
# string.
# ----------------------------------------------------------------------
word = "rat cat rat cat hat nat rat"
sub = "rat"


def count(word, sub):
    total = 0

    for i in range(len(word)-2):
        new = word[i] + word[i+1] + word[i+2]
        if new == sub:
            total = 1 + total
        else:
            total = total

    print(total)



# ----------------------------------------------------------------------
# Problem 3
# Write a function that removes the first occurrence of a string from
# another string.
# ----------------------------------------------------------------------

def removefirst(word, sub):
    total = 0
    new = ""


    for i in range(len(word)-2):
        new = word[i] + word[i+1] + word[i+2]
        if new == sub:
            if total == 0:
                new = new
                total = 1
        else:
            new = new + word[i]
        new = new + word[i]
    print(new)
removefirst(word, sub)
# ----------------------------------------------------------------------
# Problem 4
# Write a function that removes all occurrences of a string from another
# string.
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
# Problem 5
# Write a function that prints all of the separate "words" in a string.
# Print each word on a separate line. Consider a "word" to be characters
# surrounded by spaces. Use the following algorithm:
#
#    find the location of the first space
#    while there are 1 or more spaces in a string:
#         take all the characters before the space as a single word and print it
#         remove all of the characters up to and including the space
#         find the location of the next space
#
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# Problem 6
# Write a function that implements a substitution cipher. In a substitution
# cipher one letter is substituted for another to garble the message.
# For example A -> Q, B -> T, C -> G etc.  Your function should take two
# parameters, the message you want to encrypt, and a string that represents
# the mapping of the 26 letters in the alphabet. Your function should
# return a string that is the encrypted version of the message.
#
# Note the following:
#  1) The mapping "MAWBCDFHYIJKLNOGPQERSTUVXZ" means that A -> M, B -> A,
#     C -> W, D -> B, etc.
#  2) Spaces should be ignored.
#  3) Only letters should be mapped to new letters. Ignore any character that
#     is not a letter of the alphabet (a-z). (Use the string method isalpha().)
#  4) You can determine the numerical value of a letter using the ord()
#     function. To find its offset in the alphabet, subtract it from the
#     ord("A"). (Hint: convert all characters in the input string to upper case.)
#
# For example, If your message is "Now is the time for all good men to come
# to the aid of their country.", and your substitution map is
# "MAWBCDFHYIJKLNOGPQERSTUVXZ", the encrypted string would be
# "NOUYERHCRYLCDOQMKKFOOBLCNROWOLCRORHCMYBODRHCYQWOSNRQX"
#  ----------------------------------------------------------------------


# ----------------------------------------------------------------------
# Problem 7
# Write a function that decrypts the message from the previous problem.
# It should also take two parameters: the encrypted message, and the mixed
# up alphabet. The function should return a string that contains the
# original, unencrypted message, but without the spaces or any
# non-alphabetic characters.
#
# Hint: use the chr(n) function to convert a number into a character. You
# will need to offset the index of the letter by ord("A").
#  ----------------------------------------------------------------------


# ----------------------------------------------------------------------


def main():
    print("call the functions to verify their correctness.")

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()

