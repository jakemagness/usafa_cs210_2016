#!/usr/bin/env python

"""
Lab04 - Practice debugging a program to find ParseErrors, TypeErrors,
        NameErrors, and ValueErrors.

Documentation Statement: (none)

"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules
import math

# Metadata
__author__ = "Jake Magness"
__email__ = "C18Jake.Magness@usafa.edu"
__date__ = "Aug 18, 2016"

# Constants
N = 3

# Get three numbers from the user
n1 = input("First number : ")
n2 = input("Second number: ")
n3 = input("Third number: ")

# Change the variable type
n1 = float(n1)
n2 = float(n2)
n3 = float(n3)

# Calculate properties of the three numbers
sum = n1 + n2 + n3
average = sum // 3
variance = (average - n1) ** 2 + (average - n2) ** 2 + (average - n3) ** 2
standard_deviation = math.sqrt(variance)
population_standard_deviation = math.sqrt(variance / N)
sample_standard_deviation = math.sqrt(variance / (N-1))
minimum = min(n1, n2, n3)
maximum = max(n1, n2, n3)

# Display the results
print("The inputs were: ", n1, n2, n3)
print("Their average is", average)
print("Their standard deviation is", standard_deviation)
print("Their population standard deviation is", population_standard_deviation)
print("Their sample standard deviation is", sample_standard_deviation)
print("Their minimum value is", minimum)
print("Their maximum value is", maximum)

# Make a list of the errors you found.
# Include the line number, the type of error, and how you fixed it.
# Line 17, had to add a space between the + sign and n2
# Line 14 needs to include changing the variable of the inputs into floats, added statements to change to float type
# Line 37 made the statement (average - n3) multiplied by two instead of the power of two, added * to raise to power
# Line 39 has the variance variable named incorrectly, changed variable to correct name
# Line 40 needs to have parenthesis added around "N-1" in order to allow operation to be performed, parenthesis added
# Line 41, the variable n4 has never been instantiated, changed it to n3
# Line 27 has the input as "fourth number" when it in fact the third number, changed statement to third
