#!/usr/bin/env python

"""
A description of the problem being solved.

Documentation Statement:

"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules
import math

# Metadata
__author__ = "Firstname Lastname"
__email__ = "firstname.lastname@usafa.edu"
__date__ = "Aug 18, 2016"

# Constants
HELLO = "Hello World"

# Problem solution
print(HELLO)
