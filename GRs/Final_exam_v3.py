#!/usr/bin/env python

"""
Final Exam - version 3, CS210, Fall 2016
"""
# =====================================================================

# # Metadata
__author__ = "Jake Magness"
__date__ = "Dec 14, 2016"


# ======================================================================
# Part 1: Software development                            _____ / 10 pts
# ======================================================================
def software_development_answers():
    """
    Type the correct answer for each software development question
    into the appropriate string.
    """
    question_1 = "B"
    question_2 = "B"
    question_3 = "A"
    question_4 = "C"
    question_5 = "D"

    return question_1, question_2, question_3, question_4, question_5


# ======================================================================
# Problem 1                                               _____ / 30 pts
# ======================================================================

def nautical_miles_to_feet(distance):
    naut_mile = 6076.12
    statue_mile = float(5280)

    ratio = naut_mile/statue_mile
    feet = distance*ratio*5280
    param1 = float(110*statue_mile)
    param2 = float(410*statue_mile)

    if feet >= param1:
        if feet <= param2:
            return feet
        else:
            return float(0)
    else:
        return float(0)

# ======================================================================
# Problem 2                                              ______ / 30 pts
# ======================================================================


def max_hours_drive_time_permitted(duty, people, time):
    if people < 0:
        return 0
    if time > 24:
        return 0

    if duty == True:
        if people == 1:
            driving_time = 8
        else:
            driving_time = 12
    elif duty == False:
        if people == 1:
            driving_time = 8
        elif people == 2:
            driving_time = 12
        else:
            driving_time = 16
    else:
        return 0

    if driving_time + time <= 24:
        return driving_time
    else:
        driving_time = 24 - time
        return driving_time


# ======================================================================
# Problem 3                                              ______ / 30 pts
# ======================================================================


def sparse_percentage(sparse):
    zero = 0
    count = 0
    for i in range(len(sparse)):
        for n in range(len(sparse[i])):
            count += 1
            if sparse[i][n] == 0:
                zero += 1

    percent = zero/count*100
    percent = str(percent)
    slice = percent.split(".")
    slice[1] = float(slice[1][0])
    if slice[1] > 5:
        percent = float(percent)
        percent += 1
        percent = str(percent)
        slice2 = percent.split(".")
        return slice2[0]
    else:
        return slice[0]


# ======================================================================
# Problem 4                                              ______ / 40 pts
# ======================================================================
class MyException(Exception):
    """ This is a custom exception. """
    def __init__(self):
        self.words = "Error"

    def __str__(self):
        return self.words

def top_40_songs():
    a_file = open("top40songs.txt", "r")
    names = [0]*40
    count = 0
    array = []
    array2 = []
    i = 0
    count = 0

    for line in a_file:
        words = line.split()
        if words[0][2] is not ")":
            if words[0][1] != "n":
                if words[0][1] != "r":
                    words[0] = words[0][1:3]
                    words[0] = int(words[0])
                    array.append(words)
                else:
                    words[0] = 99
                    words[0] = int(words[0])
                    array.append(words)
            else:
                words[0] = 99
                words[0] = int(words[0])
                array.append(words)
        else:
            words[0] = words[0][1]
            words[0] = int(words[0])
            array.append(words)

        i = 1
        name = ""
        num = words
        while num[i] != "-":
            name += num[i] + " "
            i += 1
        names[count] = name
        count += 1
        print(names)

    # I can't figure this out for some reason.... idk whats wrong
    # I just want this to sort and move each song into position. Then I just have to format it... I can't figure it out
    # I got the songs in order, i just had to get the numbers associated with the names.  THe names were sorted as well
    array2 = []
    x = 0
    for i in range(len(array)):
        for x in range(len(array)):
            if array[x][0] == i:
                array2.append(array[x])
    for i in range(len(array)):
        if array[i][0] == 99:
            array2.append(array[i])

    for i in range(len(array)):
        while array[i][x] != "-":
            array[i][1] += array[i][1] + ""
            x += 1
    print(array)





            # try:
            #     words[0] = int(words[0])
            # except MyException:
            #     print("exception")
        # else:
        #     words[0] = words[0][1:2]
        # print(words)

        # array[i] = words



    a_file.close()

# ======================================================================
# Problem 5                                              ______ / 40 pts
# ======================================================================
import random

def create_maze(filename, rows, columns):
    filename += ".txt"
    outfile = open(filename, "w")
    maze = ""
    for i in range(rows):
        for n in range(columns):
            if i == 0:
                maze += "_"
            elif i == rows-1:
                if n == 0:
                    maze += "|"
                elif n == columns-1:
                    maze += "|"
                else:
                    maze += "_"
            elif n == 0:
                maze += "|"
            elif n == columns - 1:
                if i != rows-1:
                    maze += "|"
            else:
                ranvalue = random.randrange(0, 101)
                if ranvalue == 8:
                    maze += "_"
                elif ranvalue == 16:
                    maze += "|"
                else:
                    maze += " "
        maze += "\n"
    outfile.write(maze)
    outfile.close()

# ======================================================================
# Problem 6                                              ______ / 40 pts
# ======================================================================
# Docstring for problem 6                                ______ / 10 pts
# ======================================================================

class PieChart:
    """
    A class that creates the parameters for a PieChart with basic
    functions to manipulate the data
    """
    def __init__(self, categories, values):
        if type(categories) is not list:
            self.categories = []
        elif type(values) is not list:
            self.values = []
        else:
            if len(categories) < len(values):
                num = len(values) - len(categories)
                self.categories = categories
                self.values = values
                for i in range(num):
                    self.categories.append("0")
            elif len(categories) > len(values):
                self.categories = categories
                self.values = values
                num = len(categories) - len(values)
                for i in range(num):
                    self.values.append(0)
            else:
                self.categories = categories
                self.values = values

    def add(self, new_category, value):
        self.categories.append(new_category)
        self.values.append(value)

    def remove(self, category_name):
        x = 0
        for i in range(len(self.categories)):
            if self.categories[i] == category_name:
                x = i
        self.categories.remove(category_name)
        del self.values[x]

    def __str__(self):
        word = ""
        if self.categories == []:
            return "Nothing in this object"
        else:
            for i in range(len(self.categories)):
                word += ("{}: \t {}\n".format(self.categories[i], self.values[i]))
            return word

    def draw(self):
        pass
# ======================================================================
# Problem 7                                              ______ / 20 pts
# ======================================================================

def remove_adjacent_duplicates(dna):
    counts = 0
    if len(dna) < 5:
        print(dna)
        return True
    if dna[0] == dna[1]:
        return remove_adjacent_duplicates(dna.strip(dna[0]) + dna[0])
    else:
        return remove_adjacent_duplicates(dna[2:] + dna[0:2])
# ----------------------------------------------------------------------
def main():
    print("Function tests; call functions as needed to validate their correctness.")

    # Problem 1 tests
    # blah = nautical_miles_to_feet(1)
    # print(blah)
    # blah = nautical_miles_to_feet(200)
    # print(blah)
    # blah = nautical_miles_to_feet(6000)
    # print(blah)

    # Problem 2 tests
    # test = max_hours_drive_time_permitted(True, 2, 12)
    # print(test)
    # test = max_hours_drive_time_permitted(False, 3, 6)
    # print(test)
    # test = max_hours_drive_time_permitted(True, 1, 22)
    # print(test)
    # test = max_hours_drive_time_permitted(True, -3, 22)
    # print(test)

    # Problem 3 tests
    m1 = [[0, 0, 0],
          [0, 0, 0],
          [0, 0, 0]]

    m2 = [[4, 0, 1, 0, 2],
          [0, 2, 0, 0, 0],
          [0, 0, 5, 0, 0]]

    m3 = [[0, 1],
          [5, 0],
          [6, 0],
          [0, 3],
          [8, 0]]

    # test = sparse_percentage(m1)
    # print(test)
    # test = sparse_percentage(m2)
    # print(test)
    # test = sparse_percentage(m3)
    # print(test)

    # Problem 4 tests
    top_40_songs()

    # Problem 5 tests
    # create_maze("maze", 100, 100)

    # Problem 6 tests
    # create_pie_charts()

    # Problem 7 tests
    # remove_adjacent_duplicates("AATTTTCCCGG")
    # remove_adjacent_duplicates("ATCGGGG")
    # remove_adjacent_duplicates("ATCG")
    # remove_adjacent_duplicates("ATCGATTGAGCTCTAGG")

def create_pie_charts():
    x = ["the", "thing", "word", "stuff", "and"]
    y = [0, 2, 3, 4]
    z = PieChart(x, y)
    print(z)
    z.add("test", 44)
    print(z)

    test2 = PieChart("", "")
    print(test2)

    z.remove("thing")
    print(z)

    z.draw()
# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()

