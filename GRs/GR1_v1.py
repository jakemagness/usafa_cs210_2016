#!/usr/bin/env python

"""
GR 1 - The first Graded Review in CS210, Fall 2016
"""
# =====================================================================

# # Metadata
__author__ = "Jake Magness"
__date__ = "Sept 20, 2016"

# ----------------------------------------------------------------------
# Problem 1
# ----------------------------------------------------------------------
def convert():
    feet = float(input("Feet: "))
    inches = float(input("Inches: "))
    thirtyseconds = float(input("Thirty-seconds: "))


    m = 0.3048

    meters = (feet*m) + ((inches/12)*m) + ((thirtyseconds/32/12)*m)

    feet = int(feet)
    inches = int(inches)
    thirtyseconds = int(thirtyseconds)

    feet = str(feet)
    inches = str(inches)
    thirtyseconds = str(thirtyseconds)
    meters = str(meters)

    print(feet + "' " + inches + "-" + thirtyseconds + """/32" = """ + meters + "m")

# ----------------------------------------------------------------------
# Problem 2
# ----------------------------------------------------------------------
import random

def test_random(num):
    """
    Takes an amount of numbers to test and then generates that many random numbers.  The function will print the average
    value of the random numbers generated.  The value should be around 0.5
    :param num: The amount of numbers to randomly generate for test
    :return: None
    """
    sum = float(0)

    for i in range(num):
        ran = float(random.random())
        sum = sum + ran

    avg = sum/num
    print(avg)
# ----------------------------------------------------------------------
# Problem 3
# ----------------------------------------------------------------------
def roll_dice(desired):

    condition = False
    sum = 0

    while condition == False:
        d1 = random.randrange(1, 7)
        d2 = random.randrange(1, 7)
        sum = sum + 1

        if d1 == desired:
            if d2 == desired:
                condition = True
                print(d1, d2)
                print("Rolled a double " + str(desired) + " after " + str(sum) + " rolls")
            else:
                condition = False
                print(d1, d2)
        else:
            condition = False
            print(d1, d2)



# ----------------------------------------------------------------------
# Problem 4
# ----------------------------------------------------------------------
def hurricane(speed):
    cat = 0
    if speed <= 73:
        cat = 0
    elif speed <= 95:
        cat = 1
    elif speed <= 110:
        cat = 2
    elif speed <= 129:
        cat = 3
    elif speed <= 156:
        cat = 4
    else:
        cat = 5
    return(cat)
# ----------------------------------------------------------------------
# Problem 5
#  ----------------------------------------------------------------------

def remove_vowels(sentance):
    i = len(sentance)
    new = ""


    for n in range(i):
        if sentance[n] == "a":
            new = new
        elif sentance[n] == "e":
            new = new
        elif sentance[n] == "i":
            new = new
        elif sentance[n] == "o":
            new = new
        elif sentance[n] == "u":
            new = new
        else:
            new = new + sentance[n]

    return(new)

# ----------------------------------------------------------------------
# Problem 6
#  ----------------------------------------------------------------------


def design(number_squares, circle_radius, square_size):
    from turtle import TurtleScreen, RawTurtle, TK

    CANVAS_WIDTH = 600
    CANVAS_HEIGHT = 600

    # Create a Tkinter graphics window
    graphics_window = TK.Tk()
    graphics_window.title("Lab 5 Problems")

    # Create a "canvas" inside the graphics window to draw on
    my_canvas = TK.Canvas(graphics_window, width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
    my_canvas.pack()  # Organizes my_canvas inside the graphics window

    # Create a "canvas" made specifically for turtle graphics
    turtle_canvas = TurtleScreen(my_canvas)
    turtle_canvas.bgcolor("white")

    # Create a turtle to draw on the canvas
    mary = RawTurtle(turtle_canvas)

    # If you want the drawing to be as fast as possible, uncomment these lines
    turtle_canvas.delay(0)
    mary.hideturtle()
    mary.speed("fastest")

    # ***************** Your solution goes here ****************
    angle = 360/number_squares

    for i in range(number_squares):
        mary.penup()
        mary.setheading((i-1) * angle)
        mary.forward(circle_radius)
        mary.setheading(270)
        mary.forward(square_size/2)
        mary.setheading(45)
        draw_square(mary, square_size)
        mary.penup()
        mary.goto(0, 0)


    # Keep the window open until the user closes it.
    TK.mainloop()

def draw_square(t, size):
    t.pendown()
    for i in range(4):
        t.forward(size)
        t.left(90)


# ----------------------------------------------------------------------
def main():
    sent = "This is a test"
    # convert()
    # test_random(100)
    # roll_dice(2)
    # hurricane(157)
    # remove_vowels(sent)
    # design(10, 100, 20)
    # print("Function tests; call functions as needed to validate their correctness.")

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()

