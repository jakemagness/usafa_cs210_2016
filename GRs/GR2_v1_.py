#!/usr/bin/env python

"""
GR 2 - The second Graded Review in CS210, Fall 2016
"""
# =====================================================================

# # Metadata
__author__ = "Jake Magness"
__date__ = "1 Nov 2016"

# ----------------------------------------------------------------------
# Problem 1 (25 pts)
# ----------------------------------------------------------------------
def get_data():
    a_file = open("CS210cadets.txt", "r")
    words = ""
    array = [0]*94
    i = 0

    for line in a_file:
        words = line.split()
        array[i] = words
        i += 1
    del array[0]

    for n in range(len(array)):
        for z in range(len(array[n])):
            for y in range(len(array[n][z])):
                if array[n][z][y] == ",":
                    array[n][z] = array[n][z][:y]
    for q in range(len(array)):
        if array[q][2] == "CompSci":
            array[q][2] = array[q][2] + " " + array[q][3]
            del array[q][3]
        elif array[q][4] == "CompSci":
            array[q][1] = array[q][1] + " " + array[q][2] + " " + array[q][3]
            del array[q][2]
            del array[q][2]
            array[q][2] = array[q][2] + " " + array[q][3]
            del array[q][3]

        elif array[q][3] == "CompSci":
            array[q][1] = array[q][1] + " " + array[q][2]
            del array[q][2]
            array[q][2] = array[q][2] + " " + array[q][3]
            del array[q][3]
    a_file.close()
    return array

# ----------------------------------------------------------------------
# Problem 2 (25 pts) (+5 pts for a docstring)
# ----------------------------------------------------------------------

def save_emails(students):
    """
    Takes student information that is broken into a list of lists and saves information into a file.
    :param students: List of the student information
    :return: None, saves information in a seperate file
    """
    outfile = open("just_emails.txt", "w")

    for i in range(len(students)):
        n = 0
        while students[i][4][n] != "@":
            n += 1
        outfile.write("{} {} {}".format(" " *(30-n), str(students[i][4]), "\n"))
    outfile.close()

# ----------------------------------------------------------------------
# Problem 3 (25 pts)
# ----------------------------------------------------------------------

def by_class_year(students):
    print("Firsties")
    for i in range(len(students)):
        if students[i][4][:3] == "C17":
            print(students[i][1] + ", " + students[i][0])
    print("")
    print("2-degrees")
    for i in range(len(students)):
        if students[i][4][:3] == "C18":
            print("{}, {}".format(students[i][1], students[i][0]))
    print("")
    print("3-degrees")
    for i in range(len(students)):
        if students[i][4][:3] == "C19":
            print("{}, ".format(students[i][1]) + students[i][0])


# ----------------------------------------------------------------------
# Problem 4 (25 pts)
# ----------------------------------------------------------------------

def organize_by_section(students):
    section = {"M1A": "0", "M3A": "0", "T1A": "0", "T3A": "0", "T6A": "0"}

    for i in range(len(students)):
        if students[i][3] == "M1A" and section["M1A"] == "0":
            section["M1A"] = students[i][1] + ", " + students[i][0] + "{}".format("\n")
        elif students[i][3] == "M1A" and section["M1A"] != "0":
            section["M1A"] += students[i][1] + ", " + students[i][0] + "{}".format("\n")

        if students[i][3] == "M3A" and section["M3A"] == "0":
            section["M3A"] = students[i][1] + ", " + students[i][0] + "{}".format("\n")
        elif students[i][3] == "M3A" and section["M3A"] != "0":
            section["M3A"] += students[i][1] + ", " + students[i][0] + "{}".format("\n")

        if students[i][3] == "T1A" and section["T1A"] == "0":
            section["T1A"] = students[i][1] + ", " + students[i][0] + "{}".format("\n")
        elif students[i][3] == "T1A" and section["T1A"] != "0":
            section["T1A"] += students[i][1] + ", " + students[i][0] + "{}".format("\n")

        if students[i][3] == "T3A" and section["T3A"] == "0":
            section["T3A"] = students[i][1] + ", " + students[i][0] + "{}".format("\n")
        elif students[i][3] == "T3A" and section["T3A"] != "0":
            section["T3A"] += students[i][1] + ", " + students[i][0] + "{}".format("\n")

        if students[i][3] == "T6A" and section["T6A"] == "0":
            section["T6A"] = students[i][1] + ", " + students[i][0] + "{}".format("\n")
        elif students[i][3] == "T6A" and section["T6A"] != "0":
            section["T6A"] += students[i][1] + ", " + students[i][0] + "{}".format("\n")

    print("M1A")
    print(section["M1A"])
    print("M3A")
    print(section["M3A"])
    print("T1A")
    print(section["T1A"])
    print("T3A")
    print(section["T3A"])
    print("T6A")
    print(section["T6A"])
# ----------------------------------------------------------------------
# Problem 5 (20 pts) (recursive function)
#  ----------------------------------------------------------------------

def multiplication(numb, times):

    if times == 0:
        return 0
    elif times == 1:
        return numb
    else:
        return numb + multiplication(numb, times - 1)

# ----------------------------------------------------------------------


def main():
    print("Function tests")
    students = get_data()
    save_emails(students)
    # by_class_year(students)
    # organize_by_section(students)
    # answer = multiplication(6, 15)
    # print(answer)
# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
