#!/usr/bin/env python

"""
Create a squadron patch that is centered and fits the user defined size.

Documentation Statement: None.

"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules
import math
from turtle import TurtleScreen, RawTurtle, TK

# Metadata
__author__ = "Jake Magness"
__email__ = "C18Jake.Magness@usafa.edu"
__date__ = "Aug 31, 2016"

from math import pi

# Constants
CANVAS_WIDTH = 600
CANVAS_HEIGHT = int(CANVAS_WIDTH * 9 / 16)  # Produces the eye-pleasing 16:9 HD aspect ratio.

# Create a Tkinter graphics window
graphics_window = TK.Tk()
graphics_window.title("WINDOW NAME")

# Create a "canvas" inside the graphics window to draw on
my_canvas = TK.Canvas(graphics_window, width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
my_canvas.pack()  # Organizes my_canvas inside the graphics window

# Create a "canvas" made specifically for turtle graphics
turtle_canvas = TurtleScreen(my_canvas)
turtle_canvas.bgcolor("white")

# Create a turtle to draw on the canvas
sammy = RawTurtle(turtle_canvas)

# If you want the drawing to be as fast as possible, uncomment these lines
turtle_canvas.delay(0)
sammy.hideturtle()
sammy.speed("fastest")

# Draw squadron NN patch (your code goes here)

# User input
size = input("Enter the size of the patch: ")
size = int(size)

# Orient the turtle in a location that when the circle is drawn will make the circle appear centered
sammy.penup()
sammy.goto(0, 0)
sammy.forward(size/2)
sammy.left(90)
sammy.pencolor("yellow")
sammy.pensize(2)
sammy.pendown()

# Fill the circle in with a background color
sammy.begin_fill()
sammy.fillcolor("lightblue")

#Creates a loop to draw the circle
for i in range (100):
    up = float(size*.06)
    sammy.forward(up)
    sammy.left(6.6)

# Ends the area for the polygon
sammy.end_fill()
sammy.penup()
sammy.goto(0,0)
# Create squadron number section

sammy.pencolor("red")

# Create x and y variables to find proper position
xpos = int(size/2*(.95))
ypos = -int(size/2*(1/4))

# Go into position to begin drawing.  Position is based on the size of the circle defined by user
sammy.goto(xpos,ypos)
sammy.pendown()
sammy.pensize(2)

# Orient to begin drawing
sammy.setheading(180)
sammy.forward(size)

# Set bxpos and bypos for bot of the x and y position for the horizontal line
xbpos = -xpos + (size*.04) # To get diagonal that roughly matches edge of circle
ybpos = ypos - (size*.15)
sammy.goto(xbpos, ybpos)
sammy.setheading(0)
sammy.forward(size*.81)
sammy.goto(xpos, ypos)

# Redraw border of circle to clear the red off the lines
sammy.penup()
sammy.goto(0, 0) # Get back into starting position to draw circle
sammy.setheading(0)
sammy.forward(size/2) # Repeating steps from beginning to draw circle
sammy.left(90)
sammy.pencolor("yellow")
sammy.pensize(7)
sammy.pendown()

for i in range (100):
    up = float(size*.06)
    sammy.forward(up)
    sammy.left(6.6)

sammy.pensize(1)
# Begin drawing the Roman numberals within the box that was created.
sammy.penup()
sammy.goto(xpos, ypos)
sammy.setheading(180)
sammy.pencolor("red")
sammy.forward(size/6) # Changing pen color and moving proper distance into block to begin drawing roman numerals

# Begin drawing V
sammy.pendown()
sammy.pencolor("red")
sammy.setheading(235)
sammy.forward(size*.18)
sammy.setheading(125)
sammy.forward(size*.18)

# Draw X
sammy.setheading(215)
sammy.forward(size*.26)
sammy.setheading(90)
sammy.penup()
sammy.forward(size*.15)
sammy.pendown()
sammy.setheading(325)
sammy.forward(size*.25)

# Second X

# Reorient Sammy to use the same code as previous X
sammy.penup()
sammy.setheading(180)
sammy.forward(size*.2)
sammy.setheading(90)
sammy.forward(size*.14)
sammy.pendown()

# Drawing the second x
sammy.setheading(215)
sammy.forward(size*.25)
sammy.setheading(90)
sammy.penup()
sammy.forward(size*.15)
sammy.pendown()
sammy.setheading(325)
sammy.forward(size*.25)

# Send Sammy right hand side of the patch to draw lines
xlinepos = xpos + size*.01
ylinepos = ypos + size*.04

# Lift pen and change color then move to line starting position
sammy.penup()
sammy.pencolor("yellow")
sammy.goto(xlinepos, ylinepos)

# Set heading to for the first line
sammy.setheading(170)
sammy.pendown()
sammy.forward(size*.85)
sammy.setheading(50)
sammy.forward(size*.25)
sammy.color("black")
sammy.stamp()
sammy.penup()

# Return to start and begin second line
sammy.goto(xlinepos, ylinepos)
sammy.pendown()
sammy.pencolor("yellow")
sammy.setheading(175)
sammy.pendown()
sammy.forward(size*.95)
sammy.setheading(55)
sammy.forward(size*.25)
sammy.color("black")
sammy.stamp()
sammy.penup()

# Draw the cat
sammy.goto(xpos, ypos + 2) # Get in proper position
sammy.setheading(180) # Orientation
sammy.forward(size*.1) # Out a distance to begin drawing back leg

# Back leg
sammy.pendown()
sammy.begin_fill()
sammy.forward(size*.06) # Leg width
sammy.setheading(90)
sammy.forward(size*.15) # Leg height
bodystart = sammy.position() # Get position as reference point for body
sammy.setheading(0)
sammy.forward(size*.06) # Leg width
sammy.setheading(270)
sammy.forward(size*.15) # Leg height
sammy.end_fill()

# Draw body
sammy.penup()
sammy.goto(size*.315, size*.035)
sammy.pendown()
sammy.begin_fill()
sammy.setheading(170)
sammy.forward(size*.4)
frlegstart = sammy.position() # Get reference point for front leg
sammy.setheading(90)
sammy.forward(size*.3)
headstart = sammy.position() # Get reference point to draw head
sammy.setheading(0)
sammy.forward(size*.05)
sammy.setheading(270)
sammy.forward(size*.2)
sammy.setheading(350)
sammy.forward(size*.41)
sammy.setheading(270)
sammy.forward(size*.1)
sammy.end_fill()

# Draw front left leg
sammy.goto(-size*.08, size*.11)
sammy.begin_fill()
sammy.setheading(270)
sammy.forward(size*.22 + 1)
sammy.setheading(0)
sammy.forward(size*.04)
sammy.setheading(90)
sammy.forward(size*.22 + 1) # Need to add a pixel to make leg look correct)
sammy.goto(-size*.075, size*.11)
sammy.end_fill()

# Draw middle leg
sammy.goto(size*.01, size*.11)
sammy.begin_fill()
sammy.setheading(280)
sammy.forward(size*.225 + 1) # Comes up a little short so accounting for it by adding a pixel
sammy.setheading(0)
sammy.forward(size*.03)
sammy.setheading(100)
sammy.forward(size*.225 + 1)
sammy.goto(size*.01, size*.11)
sammy.end_fill()

# Draw head
sammy.penup()
sammy.goto(-size*.01 , size*.445)

# Loop to draw head
sammy.begin_fill()
sammy.pendown()

for i in range (50):
    up = float(size*.007)
    sammy.forward(up)
    sammy.left(7.6)

sammy.end_fill()

# Draw eye
sammy.penup()
sammy.goto(-size*.03, size*.46)
sammy.pencolor("yellow")
sammy.fillcolor("yellow")
sammy.begin_fill()
sammy.pendown()

for i in range (50):
    up = float(size*.001)
    sammy.forward(up)
    sammy.left(7.6)

sammy.end_fill()

# Draw iris

sammy.goto(-size*.03, size*.46)
sammy.pencolor("red")
sammy.fillcolor("red")
sammy.begin_fill()

for i in range (100):
    up = float(size*.0007)
    sammy.forward(up)
    sammy.left(7.6)

sammy.end_fill()

# Draw ears
sammy.color("black")
sammy.penup()
sammy.goto(-size*.038, size*.51)
sammy.setheading(90)
sammy.stamp()

sammy.goto(-size*.085, size*.51)
sammy.stamp()

# Draw tail
sammy.pensize(3)
sammy.goto(size*.37, size*.13)
sammy.pendown()
sammy.forward(size*.15)
sammy.setheading(300)
sammy.forward(size*.1)

# Keep the window open until the user closes it.
TK.mainloop()