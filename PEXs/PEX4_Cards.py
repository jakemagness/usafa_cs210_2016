#!/usr/bin/env python

"""
An ordered list of cards in a RACK-O game.

Documentation Statement: None.

"""
# =====================================================================
import random

# Metadata
__author__ = "Jake Magness"
__email__ = "C18Jake.Magness@usafa.edu"
__date__ = "11 Nov 2016"


class Cards:
    """ Cards represents an ordered list of cards """
    cards_list = []

    def __init__(self, first_card=0, last_card=0):
        """
        Creates a deck of cards in numerical order from the first card to the last card.
        :param first_card: The first card in a deck of cards
        :param last_card: The last card in a deck of cards
        """
        self.counter = 0
        if first_card == 0 and last_card == 0:
            self.cards_list = []
        else:
            self.initialize(first_card, last_card)

    def initialize(self, first_card, last_card):
        """
        Used to re-initalize the card class.  Could be used in a situation when the round resets.  My code did not
        require the use of this funciton
        :param first_card: The first card in a deck of cards
        :param last_card: The last card in a deck of cards
        :return:
        """
        n = 0
        length = last_card - first_card + 1
        hand = [0] * length
        while first_card <= last_card:
            hand[n] = first_card
            n += 1
            first_card += 1

        self.cards_list = hand

    def get_top_card(self):
        """
        Gets the top card from the deck
        :return: Returns the card value, if the list is empty, returns -1
        """
        if len(self.cards_list) == 0:
            return -1
        else:
            return self.cards_list[0]

    def get_card_at(self, position):
        """
        Gets a card at a certain position in the Card object array
        :param position: The position of the card
        :return: The value of the card
        """
        if position > len(self.cards_list)-1:
            return -1
        elif position-1 < -len(self.cards_list):
            return -1
        else:
            return self.cards_list[position]

    def add_to_end(self, new_card):
        """
        Adds a card to the end/bottom of the deck
        :param new_card: The card to be added
        :return: Does not return a value
        """
        self.cards_list.append(new_card)

    def add_to_top(self, new_card):
        """
        Adds a card to the top of the deck
        :param new_card: Value of new card to be added
        :return: Does not return a value
        """
        self.cards_list.insert(0, new_card)

    def replace(self, position, new_card):
        """
        Replaces a card at a position with a new card
        :param position: The position of card to be changed
        :param new_card: The value of the new card
        :return: Does not return a value
        """
        if 0 <= position < len(self.cards_list):
            self.cards_list[position] = new_card

    def remove_top_card(self):
        """
        Removes the card from position 0/top of deck
        :return: Nothing
        """
        if len(self.cards_list) > 0:
            card = self.cards_list[0]
            del self.cards_list[0]
            return card
        else:
            return -1

    def find(self, card):
        """
        Finds a card of a certain value in the Card Object
        :param card: The value of the card
        :return: Returns the position of the card or -1 if the card is not found
        """
        found_card = False
        i = 0

        while i < len(self.cards_list) and not found_card:
            if self.cards_list[i] == card:
                return i
            i += 1

        return -1

    def shuffle(self):
        """
        Shuffles the deck
        :return: Does not return a value
        """
        import random

        count = len(self.cards_list)

        # Creating empty deck to store values in
        new_deck = [None] * count

        # For loop that executes the fisher-yates shuffle
        for i in range(count):
            ranvalue = random.randrange(0, count)

            new_deck[i] = self.cards_list[ranvalue]
            del (self.cards_list[ranvalue])
            count -= 1
        self.cards_list = new_deck

    def __str__(self):
        """
        Defines how a card object will be displayed if printed
        :return: What will be printed
        """
        if len(self.cards_list) > 0:
            return ("\nThe first card in this deck is {}. \nThe last card in this deck is {}. \nThe entire deck is: {}"
                .format(self.cards_list[0], self.cards_list[len(self.cards_list)-1], self.cards_list))
        else:
            return ""

    def __contains__(self, card):
        """
        Checks to see if the Card object contains the card inputted
        :param card: The card to be checked for
        :return: Returns true or false if the card is found
        """
        found_card = False
        i = 0

        while i < len(self.cards_list) and not found_card:
            if self.cards_list[i] == card:
                return True
            i += 1

        return False

    def __len__(self):
        """
        Length of the deck
        :return: The length of the deck
        """
        return len(self.cards_list)

    def __iter__(self): # If using this in a for loop
        """
        Used if obejct is being used in a loop
        :return: Returns itself
        """
        self.counter = 0
        return self

    def __next__(self):
        """
        Used for iterations in loop
        :return: Returns the result of the iteration
        """
        if self.counter < len(self.cards_list):
            result = self.cards_list[self.counter]
            self.counter += 1
            return result
        else:
            raise StopIteration
