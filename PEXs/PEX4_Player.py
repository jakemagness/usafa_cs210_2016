#!/usr/bin/env python

"""
A Player in a RACK-O game.

Documentation: None.
"""
# =====================================================================

# Metadata
__author__ = "Jake Magness"
__email__ = "C18Jake.Mangess@usafa.edu"
__date__ = "11 Nov 2016"

# ---------------------------------------------------------------------
from PEX4_Cards import Cards


class Player:
    """ Define a player in a card game. """

    HUMAN_PLAYER = 0
    COMPUTER_PLAYER = 1
    NETWORK_PLAYER = 2

    def __init__(self, name, score=0, type=HUMAN_PLAYER):
        """
        Creates a Player Object
        :param name: The name of the player
        :param score: The score of the player
        :param type: What type of player is this
        """
        self.name = name
        self.score = score
        self.type = type
        self.hand = Cards(0, 0) # Set as a blank card type object

    def __str__(self):
        """
        Defines how the player object will be output in a string
        :return: Player object definition
        """
        return "Player {}'s hand is: {}.  Their score is {}. ".format(self.name, self.hand, self.score)

    def get_name(self):
        """
        Retrieves the players name
        :return: Player name
        """
        return self.name

    def get_hand(self):
        """
        Gets the player's hand
        :return: Returns the player's hand
        """
        return self.hand

    def set_hand(self, hand):
        """
        Sets the value of a player's hand to a blank card type list
        :param hand: The hand you wish to assign to a player
        :return: Nothing
        """
        self.hand = hand  # Set as a blank card type object

    def get_score(self):
        """
        Gets the players score
        :return: The value of the player's score
        """
        return self.score

    def reset_score(self):
        """
        Resets a player's score to zero
        :return: Nothing
        """
        self.score = 0

    def increment_score(self, amount):
        """
        Adds to the player's sore
        :param amount: The amount to be added
        :return: Nothing
        """
        self.score += amount


