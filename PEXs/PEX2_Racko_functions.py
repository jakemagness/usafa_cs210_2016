#!/usr/bin/env python

"""
C2C Jake Magness
4 October 2018

Creates functions that will be used to create a Racko Game.

Documentation Statement: None.

"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules

# Metadata
__author__ = "Jake Magness"
__email__ = "C18Jake.Magness@usafa.edu"
__date__ = "Sept 28, 2016"
# ---------------------------------------------------------------------


def create_deck(number_players):
    """
    Create a deck of cards for a RACK-O game.
    :param number_players: the number of players in a game.
    :return: a list containing sequential numbers 1 to 40, 50, or 60. Returns
    None if the number_players is invalid.
    """
    if number_players == 2:
        number_cards = 40
    elif number_players == 3:
        number_cards = 50
    elif number_players == 4:
        number_cards = 60
    else:
        print("Error: {} is an invalid number of players".format(number_players))
        return None

    card_deck = []
    for card in range(1, number_cards+1):
        card_deck.append(card)

    return card_deck

# ---------------------------------------------------------------------

def get_number_of_players():
    """
    Get user input that determines how many players will be playing the game
    :return: returns the number of players, either 2, 3 or 4.  Returns -1
    if the amount of players is invalid.
    """

    valid = False

    while valid == False:

        players = input("Enter the number of players: ")
        players = str(players)

        # Simply checks if the correct number of players are returned.
        if players == "2":
            valid = True
            return int(players)
        elif players == "3":
            valid = True
            return int(players)
        elif players == "4":
            valid = True
            return int(players)
        elif players == "quit":
            valid = True
            return -1
        else:
            print("Invalid input, please enter 2, 3 or 4 players. ")

# ---------------------------------------------------------------------


def shuffle_deck(deck):
    """
    Function shuffles the deck using the Yates Shuffle method.
    :param deck: A list that is a deck of cards is taken that is to be reordered
    :return: Reordered deck list
    """

    import random

    count = len(deck)

    # Creating empty deck to store values in
    new_deck = [None]*count

    # For loop that executes the fisher-yates shuffle
    for i in range(count):

        ranvalue = random.randrange(0, count)

        new_deck[i] = deck[ranvalue]
        del(deck[ranvalue])
        count = count - 1

    return new_deck


# ---------------------------------------------------------------------

def pass_out_cards(deck, number_players):
    """
    Passes out 10 cards to each player from an inputted deck
    :param deck: A deck that is given to be passed out to players
    :param number_players: The amount of players in the game
    :return: Returns a list of lists that contains the players and each respective hand
    """
    hands = [None]*number_players

    new_deck = [0] * 10

    for i in range(number_players):

        for n in range(10):

            new_deck[n] = deck[n + (i*10)]

        hands[i] = new_deck

        # Need to disassociate new_deck with object in order to have different values for both decks
        new_deck = [0]*10

    del deck[0:number_players*10]

    return hands


# ---------------------------------------------------------------------


def remove_top_card(deck):
    """
    Removes the top card in the deck (position 0 in the array)
    :param deck: A deck given to function that will have the top card removed
    :return: Integer value of the card that was removed from the top of the deck
    """
    if len(deck) == 0:
        return -1
    else:
        top = len(deck)
        card = deck[top-1]
        del deck[top-1]
        return card

# ---------------------------------------------------------------------

def append_card_to_deck(deck, value):
    """
    Adds one card to the end of the deck
    :param deck: The deck in which the user wants to add a card to
    :param value: The value of the card
    :return: Nothing
    """

    deck.append(value)



# ---------------------------------------------------------------------

def player_hand_is_racko(player_hand):
    """
    Determines if the players hand is a RAACKO.  Checks to see if all cards are in order with the 10 cards
    :param player_hand: List of 10 cards from the player
    :return: Returns a boolean expression whether that states if the player's hand has a racko
    """
    racko = False
    count = 1
    i = 0

    # Loop that checks all elements of the hand to see if all numbers are in order.
    while i < 9:

        if player_hand[i] < (player_hand[i+1]):
            count += 1

        i += 1

    if count == 10:
        racko = True

    return racko


# ---------------------------------------------------------------------


def player_hand_value(player_hand, winner):
    """
    Determines the player's score.  Each card in order grants 5 points
    :param player_hand: The list of cards that the player is holding
    :param winner: Whether this player was the winner of the round or not
    :return: Returns the value of the players score
    """

    if winner == True:

        return 75

    else:

        score = 5
        i = 0
        ok_to_compare_cards = True
        while i < 9 and ok_to_compare_cards:

            # Checks to see if the current value + 1 and the next card are equivalent, meaning they are in order
            if player_hand[i] < (player_hand[i + 1]):
                score += 5
            else:
                ok_to_compare_cards = False

            i += 1

    return score



