#!/usr/bin/env python

"""
PEX 5 - A Rack-o game using a GUI.
Doc: Talked to Dr.Brown in class about how to display game/round completion messages.
Asked Dr.Brown how to unbind/bind button.
"""
# =====================================================================

import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from tkinter import simpledialog
import os  # to be able to get the current working directory; os.getcwd()
import pickle
from PEX5_RACKO_game import RackoGame, GAME_OVER
from tkinter import ttk

# Metadata
__author__ = "Jake Magness"
__email__ = "C18Jake.Magness@usafa.edu"
__date__ = "Dec 1, 2016"

class RackoGUI:
    """ A GUI interface for the game of RACK-O """

    def __init__(self):
        self.window = tk.Tk()
        # Hide the initial window until we get the number of players
        self.window.withdraw()

        # To use the tkinter dialog boxes you must have a root window.
        # Therefore, the prompt for the number of players is done after
        # the root window has been created.
        number_of_players = simpledialog.askinteger("RACKO Startup",
                                                    "How many players? (2, 3, or 4)",
                                                    parent=self.window,
                                                    minvalue=2, maxvalue=4)
        if number_of_players is None: # The user selected "Cancel"
            # Cancel this application
            self.window.destroy()
            exit()
        self.number_of_players = number_of_players
        # Create the game information
        self.game = RackoGame(number_of_players)

        # Display the window
        self.window.deiconify()
        self.window['padx'] = 20
        self.window['pady'] = 20

        # Menu Bar
        menubar = tk.Menu(self.window)

        filemenu = tk.Menu(menubar, tearoff=0)
        filemenu.add_command(label="Reset", command=self.reset)
        filemenu.add_command(label="Save", command=self.save)
        filemenu.add_command(label="Load", command=self.load)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=self.exit)
        menubar.add_cascade(label="File", menu=filemenu)

        self.window.config(menu=menubar)

        # Table Frame
        table_frame = tk.LabelFrame(self.window, text="Draw Cards", relief=tk.RIDGE)
        table_frame.grid(row=1, column=1)

        # Draw button
        self.draw_card = ttk.Button(table_frame, text="Draw Pile", width=10)
        self.draw_card.grid(row=1, column=1)
        self.draw_card.bind("<ButtonRelease-1>", self.draw_game_mode)

        # Discard button
        self.discard_card = ttk.Button(table_frame, text=self.game.discard_pile.get_top_card(), width=10)
        self.discard_card.grid(row=2, column=1)
        self.discard_card.bind("<ButtonRelease-1>", self.discard_game_mode)

        # Drawn Card pile
        self.drawn_card = ttk.Button(table_frame, text="Selected Card", width=15)
        self.drawn_card.grid(row=3, column=1)
        self.drawn_card.configure(state='disable')

        self.do_not_use = ttk.Button(table_frame, text="Return Drawn", width=15)
        self.do_not_use.grid(row=4, column=1)
        self.do_not_use.configure(state='disable')

        self.replaced_card = 0
        self.chosen_card = 0

        # Display player frames
        self.update_gui()

        # Check for Rackos
        if self.check_hands() is True:
            messagebox.showinfo("Game Status", self.game.round_is_finished())
            self.update_gui()
            for i in range(self.number_of_players):
                if self.game.players[i].get_score() > 500:
                    print("here")
                    self.game_over()


    def game_over(self):
        """
        Changes status of all buttons in the game and unbinds them.
        :return: None.
        """
        for x in range(10):
            self.card[(self.game.active_player*10) + x].configure(state='disable')
            self.card[(self.game.active_player*10) + x].bind("<ButtonRelease-1>", self.swap_card)
            self.draw_card.configure(state="disable")
            self.draw_card.unbind("<ButtonRelease-1>")
            self.discard_card.configure(state='disable')
            self.discard_card.unbind("<ButtonRelease-1>")

    def draw_game_mode(self, event):
        """
        Simply used to call the take_discard_card.  Had more functionality when game mode constants were at play.
        :param event:
        :return: None.
        """
        self.take_draw_card()

    def take_draw_card(self):
        """
        Enables players for the turn after selecting the draw deck and then handles all button operations
        :return: None.
        """
        self.do_not_use.configure(state='enable')
        self.do_not_use.bind("<ButtonRelease-1>", self.return_card)
        for x in range(10):
            self.card[(self.game.active_player*10) + x].configure(state='enable')
            self.card[(self.game.active_player*10) + x].bind("<ButtonRelease-1>", self.swap_card)
        self.chosen_card = self.game.draw_pile.get_top_card()
        self.game.draw_pile.remove_top_card()
        self.drawn_card.configure(text=self.chosen_card)
        self.discard_card.configure(state="disable")
        self.discard_card.unbind("<ButtonRelease-1>")
        self.draw_card.configure(state="disable")
        self.draw_card.unbind("<ButtonRelease-1>")


    def discard_game_mode(self, event):
        """
        Simply used to call the take_discard_card.  Had more functionality when game mode constants were at play.
        :param event:
        :return: None.
        """
        self.take_discard_card()

    def take_discard_card(self):
        """
        Enables players for the turn after selecting the discard deck and then handles all button operations.
        :return: None.
        """
        for x in range(10):
            self.card[(self.game.active_player*10) + x].configure(state='enable')
            self.card[(self.game.active_player*10) + x].bind("<ButtonRelease-1>", self.swap_card)
        self.chosen_card = self.game.discard_pile.get_top_card()
        self.drawn_card.configure(text=self.chosen_card)
        self.game.discard_pile.remove_top_card()
        new_top = self.game.discard_pile.get_top_card()
        self.discard_card.configure(text=new_top)
        self.draw_card.configure(state="disable")
        self.draw_card.unbind("<ButtonRelease-1>")
        self.discard_card.configure(state='disable')
        self.discard_card.unbind("<ButtonRelease-1>")

    def swap_card(self, event):
        """
        Swaps a card from one of the decks with a player's selected card in the hand.
        :param event:
        :return: None.
        """
        # Gets location in deck of selected card
        swap = self.game.players[self.game.active_player].get_hand().find(event.widget['text'])
        x = self.game.players[self.game.active_player].hand.cards_list[swap]
        self.game.discard_pile.add_to_top(x)
        self.game.players[self.game.active_player].get_hand().replace(swap, self.chosen_card)
        self.drawn_card.configure(text="Selected Card")
        self.card[(self.game.active_player*10)+swap].configure(text=self.chosen_card)
        self.discard_card.configure(text=x)
        self.do_not_use.configure(state='disable')
        self.do_not_use.unbind("<ButtonRelease-1>")

        if self.check_hands() is False:
            self.change_turns()
        else:
            messagebox.showinfo("Game Status", self.game.round_is_finished())
            for i in range(self.number_of_players):
                print("here")
                if self.game.players[i].get_score() > 500:
                    self.game_over()

    def return_card(self, event):
        """
        Returns a drawn card to the discard pile.
        :param event:
        :return: None.
        """
        self.game.discard_pile.add_to_top(self.chosen_card)
        self.discard_card.configure(text=self.chosen_card)
        self.drawn_card.configure(text='Selected Card')
        self.do_not_use.configure(state='disable')
        self.do_not_use.unbind("<ButtonRelease-1>")
        self.change_turns()

    def check_hands(self):
        """
        Checks the hand for RACKO
        :return: Returns True if hand is racko and false is hand is not RACKO
        """
        if self.game.active_players_hand_is_racko() is True:
            return True
        else:
            return False

    def change_turns(self):
        """
        Used to change the player turn
        :return: None.
        """
        if self.check_hands() is False:
            for i in range(10):
                self.card[(self.game.active_player * 10) + i].configure(state='disable')
                self.card[(self.game.active_player * 10) + i].unbind("<ButtonRelease-1>")
        else:
            messagebox.showinfo("Game Status", self.game.round_is_finished())
            self.game_over()

        if self.game.active_player == self.game.number_of_players-1:
            self.game.active_player = 0
        else:
            self.game.active_player += 1

        self.discard_card.configure(state='enable')
        self.discard_card.bind("<ButtonRelease-1>", self.discard_game_mode)
        self.draw_card.configure(state="enable")
        self.draw_card.bind("<ButtonRelease-1>", self.draw_game_mode)

        # Update the draw pile if it is empty 
        if self.game.draw_pile is []:
            self.game.update_draw_pile()
            self.draw_card.configure(text=self.game.draw_pile.get_card_at(0))
            self.discard_card.configure(text=self.game.discard_pile.get_card_at(0))

    def reset(self):
        """
        Resets the game to a fresh game.
        :return: None.
        """
        self.window.destroy()
        program = RackoGUI()

    def exit(self):
        """
        Exits the game.
        :return: None.
        """
        answer = messagebox.askyesno("Quit?", "Are you sure you want to exit the game?")
        self.window.destroy()

    def save(self):
        """
        Saves the game file using the .pickle python functionality.
        :return:
        """
        my_filetypes = [('all files', '.*'), ('text files', '.txt')]
        name = filedialog.asksaveasfilename(parent=self.window,
                                      initialdir=os.getcwd(),
                                      title="Please select a file name for saving:",
                                      filetypes=my_filetypes)
        self.game.save_game(name)

    def load(self):
        """
        Loads the game file using the .pickle python functionality.
        :return: None.
        """
        my_filetypes = [('all files', '.*'), ('text files', '.txt')]
        name = filedialog.askopenfilename(parent=self.window,
                                    initialdir=os.getcwd(),
                                    title="Please select a file:",
                                    filetypes=my_filetypes)
        x = self.game.load_saved_racko_game(name)
        self.game = x
        self.number_of_players = self.game.number_of_players
        self.update_gui()

    def update_gui(self):
        """
        Updates the GUI at the start of the game and after loading a game.
        :return: None.
        """
        self.player_frame = [0]*self.game.number_of_players
        self.card = [0]*self.number_of_players*10
        for i in range(self.number_of_players):
            if i < 2:
                self.player_frame[i] = ttk.LabelFrame(self.window, text=self.game.players[i].get_name(),
                                                      relief=tk.RIDGE)
                self.player_frame[i].grid(row=1, column=i*4)
            else:
                self.player_frame[i] = ttk.LabelFrame(self.window, text=self.game.players[i].get_name(),
                                                      relief=tk.RIDGE)
                self.player_frame[i].grid(row=(i-2)*2, column=1)

            for n in range(len(self.game.players[i].get_hand())):
                self.card[(i*10) + n] = ttk.Button(self.player_frame[i], text=self.game.players[i].get_hand().
                                                   get_card_at(n), state='disable', width=10)
                self.card[(i*10) + n].grid(row=n+2, column=0, sticky=tk.W, pady=4)


def main():
    # Create the GUI program
    program = RackoGUI()
    # Start the GUI event loop
    program.window.mainloop()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
