#!/usr/bin/env python

"""
EXPLAIN THE PROBLEM

Documentation Statement:

"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules
import math
from turtle import TurtleScreen, RawTurtle, TK

# Metadata
__author__ = "Jake Magness"
__email__ = "C18Jake.Magness@usafa.edu"
__date__ = "Aug 31, 2016"

# Constants
CANVAS_WIDTH = 600
CANVAS_HEIGHT = int(CANVAS_WIDTH * 9 / 16)  # Produces the eye-pleasing 16:9 HD aspect ratio.

# Create a Tkinter graphics window
graphics_window = TK.Tk()
graphics_window.title("WINDOW NAME")

# Create a "canvas" inside the graphics window to draw on
my_canvas = TK.Canvas(graphics_window, width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
my_canvas.pack()  # Organizes my_canvas inside the graphics window

# Create a "canvas" made specifically for turtle graphics
turtle_canvas = TurtleScreen(my_canvas)
turtle_canvas.bgcolor("white")

# Create a turtle to draw on the canvas
sammy = RawTurtle(turtle_canvas)

# If you want the drawing to be as fast as possible, uncomment these lines
turtle_canvas.delay(0)
sammy.hideturtle()
sammy.speed("fastest")

# Draw squadron NN patch (your code goes here)

# Orient the turtle in a location that when the circle is drawn will make the circle appear centered
sammy.penup()
sammy.right(90)
sammy.forward(40)
sammy.left(90)
sammy.pencolor("yellow")
sammy.pensize(2)
sammy.pendown()

# Fill the circle in with a background color
sammy.begin_fill()
sammy.fillcolor("lightblue")

#Creates a loop to draw the circle
for i in range (90):
    sammy.forward(5)
    sammy.left(5)

# Ends the area for the polygon
sammy.end_fill()

# Create squadron number section
sammy.pencolor("red")

# Get turtle in position to begin drawing
sammy.penup()
sammy.goto(0,-15)
sammy.pendown()
sammy.pensize(2)

# Get appropriate angle to be oriented horizontally then begin drawing lines
sammy.left(90)
sammy.forward(46)
sammy.backward(96)

# Draws the bottom line of the squadron number section
sammy.goto(40,-25)
sammy.forward(76)
sammy.goto(-45,-15)

#Begin drawing the Roman numberals within the box that was created.



# Keep the window open until the user closes it.
TK.mainloop()
