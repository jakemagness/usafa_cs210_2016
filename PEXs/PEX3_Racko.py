#!/usr/bin/env python

"""
Create a program that implements the functions created in PEX2 that creates a full RACKO-O game
driven from the command line.

Documentation Statement:

"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules
from PEX2_Racko_functions import create_deck, get_number_of_players, \
    shuffle_deck, pass_out_cards, remove_top_card, append_card_to_deck, \
    player_hand_is_racko, player_hand_value

# Metadata
__author__ = "Jake Magness"
__email__ = "C18Jake.Magness@usafa.edu"
__date__ = "Oct 26, 2016"

# CONSTANTS
# Options for initial input from the user
NEW_GAME = 1
LOAD_PREVIOUS_GAME = 2

# Game status
PLAY_GAME = 5
GAME_OVER = 6
QUIT_GAME = -1

# Indexes into the game list to access game data
DEALER = 0
ACTIVE_PLAYER = 1
HANDS = 2
DRAW_PILE = 3
DISCARD_PILE = 4
PLAYER_SCORES = 5
# ---------------------------------------------------------------------


def start_of_game():
    """
    Get the user preferences for starting RACK-O.
    :return: game_option - NEW_GAME, LOAD_PREVIOUS_GAME, or QUIT_GAME
    :return: number_of_players - an integer, 2, 3, or 4
    """
    # Set default return values
    game_type = 0
    number_of_players = 0

    # Does the user want to start a new game or continue a previous game?
    print("{}) Start a new game?".format(NEW_GAME))
    print("{}) Load a previous game?".format(LOAD_PREVIOUS_GAME))
    print("q) Quit?")
    while game_type == 0:
        option = input("Option: (1, 2 or q): ")
        if option.lower() == "q":
            game_type = QUIT_GAME
        elif option == "1" or option == "2":
            game_type = int(option)
        else:
            print("Invalid input. Please re-enter your choice.")

    if game_type == NEW_GAME:
        number_of_players = get_number_of_players()

    return game_type, number_of_players
# ---------------------------------------------------------------------


def save_game(deck):
    """
    Saves the game after the completion of a turn
    :param deck: The game array is input to be saved
    :return: Nothing
    """
    outfile = open("PEX3SaveFile.txt", "w")
    for i in range(len(deck)):
        line = str(deck[i])
        outfile.write(line + "\n")


# ---------------------------------------------------------------------


def load_game():
    """
    Loads a saved game
    :return: Nothing
    """
    saved_game = [0]*6
    i = 0
    items = [0]*30
    count = 0
    a_file = open('PEX3SaveFIle.txt', 'r')
    for one_line in a_file:
        words = one_line.split()
        saved_game[i] = words
        if i < 1:
            saved_game[0] = int(words[0])
        if i == 1:
            saved_game[1] = int(words[0])
        if i == 2:
            hands = [0]*(len(words))
            for x in range(len(saved_game[2])):
                items = words[x].split()
                if len(items[0]) == 5:
                    hands[count] = int(items[0][2:4])
                    count += 1
                if len(items[0]) == 4 and items[0][2] == "]":
                    hands[count] = int(items[0][:2])
                    count += 1
                if len(items[0]) == 4 and items[0][0] == "[" and items[0][1] != "[":
                    hands[count] = int(items[0][1:3])
                    count += 1
                if len(items[0]) == 4 and items[0][0:2] == "[[":
                    hands[count] = int(items[0][2])
                    count += 1
                if len(items[0]) == 3 and items[0][0] != "[" and items[0][1] != "]":
                    hands[count] = int(items[0][0:2])
                    count += 1
                if len(items[0]) == 3 and items[0][1] == "]":
                    hands[count] = int(items[0][0])
                    count += 1
                if len(items[0]) == 3 and items[0][0] == "[":
                    hands[count] = int(items[0][1])
                    count += 1
                if len(items[0]) == 2:
                    hands[count] = int(items[0][0])
                    count += 1
            hands_to_deck = int(len(hands)/10)*[0]
            b = 0
            for y in range(len(hands_to_deck)):
                if y == 0:
                    a = y
                    b = 10
                else:
                    b = (y*10) + 10
                    a = y*10
                temp_list = (hands[a:b])
                hands_to_deck[y] = temp_list
            saved_game[2] = hands_to_deck
        if i == 3:
            draw_pile_to_deck = [0]*len(words)
            for z in range(len(words)):
                if len(words[z]) == 2 and words[z][1] == ",":
                    draw_pile_to_deck[z] = int(words[z][0])
                if len(words[z]) == 2 and words[z][0] == "[":
                    draw_pile_to_deck[z] = int(words[z][1])
                if len(words[z]) == 2 and words[z][1] == "]":
                    draw_pile_to_deck[z] = int(words[z][0])
                if len(words[z]) == 3 and (words[z][2] == "," or words[z][2] == "]") and words[z][0] != "[":
                    draw_pile_to_deck[z] = int(words[z][0:2])
                if len(words[z]) == 3 and words[z][0] == "[":
                    draw_pile_to_deck[z] = int(words[z][1:2])
                if len(words[z]) == 4 and words[z][0] == "[":
                    draw_pile_to_deck[z] = int(words[z][1:3])
            saved_game[3] = draw_pile_to_deck
            saved_game[3][0] = int(saved_game[3][0])
        if i == 4:
            if (len(words)) > 1:
                discard_pile_to_deck = [0]*len(words)
                for k in range(len(words)):
                    print(words[k])
                    if len(words[k]) == 2 and words[k][1] == ",":
                        discard_pile_to_deck[k] = int(words[k][0])
                    if len(words[k]) == 2 and words[k][0] == "[":
                        discard_pile_to_deck[k] = int(words[k][1])
                    if len(words[k]) == 2 and words[k][1] == "]":
                        discard_pile_to_deck[k] = int(words[k][0])
                    if len(words[k]) == 3 and (words[k][2] == "," or words[k][2] == "]") and words[k][0] != "[":
                        discard_pile_to_deck[k] = int(words[k][0:2])
                    if len(words[k]) == 3 and words[k][0] == "[":
                        discard_pile_to_deck[k] = int(words[k][1:2])
                    if len(words[k]) == 4 and words[k][0] == "[":
                        discard_pile_to_deck[k] = int(words[k][1:3])
                saved_game[4] = discard_pile_to_deck
                saved_game[4][0] = int(saved_game[4][0])
            else:
                saved_game[4] = []
        if i == 5:
            player_scores = [0]*(len(words))
            for w in range(len(words)):
                if len(words[w]) == 2 and words[w][1] == ",":
                    player_scores[w] = int(words[w][0])
                if len(words[w]) == 2 and words[w][0] == "[":
                    player_scores[w] = int(words[w][1])
                if len(words[w]) == 2 and words[w][1] == "]":
                    player_scores[w] = int(words[w][0])
                if len(words[w]) == 3 and (words[w][2] == "," or words[w][2] == "]") and words[w][0] != "[":
                    player_scores[w] = int(words[w][0:2])
                if len(words[w]) == 3 and words[w][0] == "[":
                    player_scores[w] = int(words[w][1:2])
                if len(words[w]) == 4 and words[w][0] == "[":
                    player_scores[w] = int(words[w][1:3])
                if len(words[w]) == 4 and (words[w][3] == "," and words[w][0] != "["):
                    player_scores[w] = int(words[w][0:3])
                if len(words[w]) == 5 and (words[w][0] == "[" or words[w][5] == "]"):
                    player_scores[w] = int(words[w][1:4])
                if len(words[w]) == 4 and words[w][0] == "[":
                    player_scores[w] = int(words[w][1:3])
                if len(words[w]) == 4 and words[w][3] == "]":
                    player_scores[w] = int(words[w][0:3])
            saved_game[5] = player_scores
        i += 1
    return saved_game


# ---------------------------------------------------------------------

def initialize_game(game_type, number_of_players):
    """
    Starts the game and creates and entire game array
    :param game_type: Which type of game the player selects
    :param number_of_players: How many players are playing the game
    :return: The game arrray
    """
    if game_type == 1:
        # Creating the deck, shuffling, and passing out the cards to the players, creates the draw pile
        deck = create_deck(number_of_players)
        shuffled_deck = shuffle_deck(deck)
        draw_pile = shuffled_deck[number_of_players*10:]
        hands = pass_out_cards(shuffled_deck, number_of_players)

        # Determine the dealer for 2 players
        if number_of_players == 2:
            card1 = remove_top_card(draw_pile)
            card2 = remove_top_card(draw_pile)
            if card1 < card2:
                dealer = 0
            else:
                dealer = 1
            append_card_to_deck(draw_pile, card1)
            append_card_to_deck(draw_pile, card2)

        # Determine the dealer for 3 players
        if number_of_players == 3:
            dealer = 0
            card1 = remove_top_card(draw_pile)
            card2 = remove_top_card(draw_pile)
            card3 = remove_top_card(draw_pile)
            if card1 < card2:
                if card1 < card3:
                    dealer = 0
            if card2 < card1:
                if card2 < card3:
                    dealer = 1
            if card3 < card1:
                if card3 < card2:
                    dealer = 2
            append_card_to_deck(draw_pile, card1)
            append_card_to_deck(draw_pile, card2)
            append_card_to_deck(draw_pile, card3)

        # Determine the dealer for 4 players
        if number_of_players == 4:
            dealer = 0
            card1 = remove_top_card(draw_pile)
            card2 = remove_top_card(draw_pile)
            card3 = remove_top_card(draw_pile)
            card4 = remove_top_card(draw_pile)
            if card1 < card2:
                if card1 < card3:
                    if card1 < card4:
                        dealer = 0
            if card2 < card1:
                if card2 < card3:
                    if card2 < card4:
                        dealer = 1
            if card3 < card1:
                if card3 < card2:
                    if card3< card4:
                        dealer = 2
            if card4 < card1:
                if card4 < card2:
                    if card4 < card3:
                        dealer = 3
            append_card_to_deck(draw_pile, card1)
            append_card_to_deck(draw_pile, card2)
            append_card_to_deck(draw_pile, card3)
            append_card_to_deck(draw_pile, card4)

        # Reshuffle cards after being inserted back into deck
        draw_pile = shuffle_deck(draw_pile)

        # Initialize other parameters such as active player, the discard pile and player scores
        if dealer == number_of_players-1:
            active_player = 0
        else:
            active_player = dealer + 1
        discard_pile = []

        if number_of_players == 2:
            player_scores = [0, 0]
            game = [dealer, active_player, hands, draw_pile, discard_pile, player_scores]
            return game

        elif number_of_players == 3:
            player_scores = [0, 0, 0]
            game = [dealer, active_player, hands, draw_pile, discard_pile, player_scores]
            return game

        else:
            player_scores = [0, 0, 0, 0]
            game = [dealer, active_player, hands, draw_pile, discard_pile, player_scores]
            return game

    if game_type == 2:
        return load_game()

    if game_type == "q":
        return -1


# ---------------------------------------------------------------------


def play_hand(game):
    """
    Plays the hand of one player of the game
    :param game: The entire game array
    :return: Returns 5 to continue playing or -1 to quit
    """
    move = "0"
    valid = True
    index = game[ACTIVE_PLAYER]
    status = 0
    save_game(game)

    while valid == True:
        index = game[ACTIVE_PLAYER]
        if game[DISCARD_PILE] != []:
            print("The card on the Draw Pile is: {} ".format(game[DRAW_PILE][len(game[DRAW_PILE]) - 1]))
            print("The card on the Discard Pile is: {}".format(game[DISCARD_PILE][len(game[DISCARD_PILE]) - 1]))
            print("Player {}, your hand is {}".format(game[ACTIVE_PLAYER], game[HANDS][index]))
            move = input("Player {}, pick a card from the Discard Pile(0) or the Draw Pile (1): "
                         .format(game[ACTIVE_PLAYER]))
            if move == "q":
                return -1
            if move == "s":
                print("Game saved!")

            if move != "0" and move != "1" and move != "s":
                print("Invalid input, enter either 0 or 1.")
                valid = True
            if move == "1":
                print("Your hand is : {}".format(game[HANDS][index]))
                discard = input("Select the card you would wish to discard (0-9): ")
                if discard == "q":
                    return -1
                discard = int(discard)
                card_to_add = game[HANDS][index][discard]
                append_card_to_deck(game[DISCARD_PILE], card_to_add)
                game[HANDS][index][discard] = remove_top_card(game[DRAW_PILE])
                print("Your hand is : {}".format(game[HANDS][index]))
                valid = False
                return 5
            if move == "0":
                print("Your hand is : {}".format(game[HANDS][index]))
                discard = input("Select the card you would wish to discard (0-9): ")
                if discard == "q":
                    return -1
                discard = int(discard)
                card_to_add = game[HANDS][index][discard]
                game[HANDS][index][discard] = remove_top_card(game[DISCARD_PILE])
                append_card_to_deck(game[DISCARD_PILE], card_to_add)
                print("Your hand is : {}".format(game[HANDS][index]))
                valid = False
                return 5
        else:
            move = input("Player {}, pick a card from the Draw Pile (1): ".format(game[1]))
            if move == "1":
                print("Your hand is : {}".format(game[HANDS][index]))
                # Input card that player wishes to discard
                discard = input("Select the card you would wish to discard (0-9): ")
                if discard == "q":
                    return -1
                discard = int(discard)
                # Adding and removing cards where appropriate and showing the which player is active
                card_to_add = game[HANDS][index][discard]
                append_card_to_deck(game[DISCARD_PILE], card_to_add)
                game[HANDS][index][discard] = remove_top_card(game[DRAW_PILE])
                print("Your hand is : {}".format(game[HANDS][index]))
                valid = False
                return 5
            if move == "q":
                return -1
            if move != "1" and move != "s":
                print("Invalid input, enter 1.")
                valid = True
                return 5
            if move == "s":
                print("Game saved!")

# ---------------------------------------------------------------------

def update_player_scores(game):
    """
    Updates the scores of the players
    :param game: The array of the entire game
    :return: Nothing, changes contents of game array
    """
    index = game[ACTIVE_PLAYER]

    for i in range(len(game[HANDS])):
        game[PLAYER_SCORES][i] += player_hand_value(game[HANDS][i], False)

    print("The current scores for the game are as follows: ")

    for n in range(len(game[HANDS])):
        print("Player {} has a score of {}".format(n, game[PLAYER_SCORES][n]))


# ---------------------------------------------------------------------


def display_round_winner(game):
    """
    Shows the winner of the round
    :param game: The entire game array
    :return:
    """
    index = game[ACTIVE_PLAYER]
    round_winner = max(game[PLAYER_SCORES])
    print("The winner for the round is Player: {} with a score of {}".format(index, round_winner))


# ---------------------------------------------------------------------


def reset_for_next_round(game):
    """
    Prepares the game for the next round
    :param game: The entire game array
    :return: Nothing, changes the contents of the deck
    """
    # Determine the dealer and active player
    if game[DEALER] != len(game[HANDS])-1:
        game[DEALER] += 1
    else:
        game[DEALER] = 0
    if game[ACTIVE_PLAYER] != len(game[HANDS]) - 1:
        game[ACTIVE_PLAYER] += 1
    else:
        game[ACTIVE_PLAYER] = 0

    # Create a new deck then put it into the game array
    deck = create_deck(len(game[HANDS]))
    shuffled_deck = shuffle_deck(deck)
    game[DRAW_PILE] = shuffled_deck[len(game[HANDS]) * 10:]
    game[HANDS] = pass_out_cards(shuffled_deck, len(game[HANDS]))
    game[DISCARD_PILE] = []


# ---------------------------------------------------------------------


def display_game_winner(game):
    """
    Shows the winner of the entire game
    :param score: The score of the winning player
    :return: Nothing
    """
    index = game[ACTIVE_PLAYER]
    print("WINNER!!!! Player {} won with a score of: {}".format(index, game[PLAYER_SCORES][index]))

# ---------------------------------------------------------------------


def main():
    """
    Play the game of RACK-O.
    :return: None

    The data of a RACK-O game is stored in a nested list as follows:
    game = [ dealer, current_player, hands, draw_pile, discard_pile, player_scores ]
      where,
         dealer is an integer in the range [0,number_players-1]
         current_player is an integer in the range [0,num_players-1]
         hands is a list of lists [ hand0, hand1, hand2, hand3 ]
                Note: each hand[0-3] is a list of 10 integers
         draw_pile is a list of 0 or more integers
         discard_pile is a list of 0 or more integers
         player_scores is a list of integers, one score per player

    Here is full description of a game for 3 players:
    game = [ 0,  # DEALER
             1,  # ACTIVE_PLAYER
             [ [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],         # HANDS
               [11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
               [21, 22, 23, 24, 25, 26, 27, 28, 29, 30] ],
             [ 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,  # DRAW_PILE
               41, 42, 43, 44, 45, 46, 47, 48, 49, 50],
             [],  # DISCARD_PILE
             [ 0, 0, 0]  # PLAYER_SCORES
           ]
    """

    # Get initial user option: start a new game or load a previous game?
    game_type, number_of_players = start_of_game()
    if game_type != QUIT_GAME:

        # Initialize a RACK-O game, either from a file, or start a new game
        game = initialize_game(game_type, number_of_players)
        # Play the game
        status = PLAY_GAME
        while status == PLAY_GAME:

            # Current player takes a turn
            status = play_hand(game)
            if status == PLAY_GAME:
                index = game[ACTIVE_PLAYER]
                # Check for a winner
                if player_hand_is_racko(game[HANDS][index]):
                    update_player_scores(game)
                    display_round_winner(game)

                    # Is the entire game over?
                    if max(game[PLAYER_SCORES]) >= 500:
                        status = GAME_OVER
                    else:
                        reset_for_next_round(game)
                else:
                    # Switch active player
                    if game[ACTIVE_PLAYER] == len(game[HANDS])-1:
                        game[ACTIVE_PLAYER] = 0
                    else:
                        game[ACTIVE_PLAYER] += 1
                    # game[ACTIVE_PLAYER] = (game[ACTIVE_PLAYER] + 1) % number_of_players

        # The game is over; display the game winner.
        if status == GAME_OVER:
            display_game_winner(game)

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()