#!/usr/bin/env python

"""
Play the game RACK-O.

Documentation: Reviewed the PEX with Dr. Brown during EI.
"""
# =====================================================================

from PEX4_RACKO_interface import get_number_of_players, get_how_to_start_game, \
    get_draw_card_choice, get_discard_choice
from PEX4_RACKO_interface import QUIT_GAME, NEW_GAME, LOAD_PREVIOUS_GAME, SAVE_GAME, \
    DRAW_FROM_THE_DISCARD_PILE, DRAW_FROM_THE_DRAW_PILE
from PEX4_Cards import Cards
from PEX4_Player import Player
import pickle
import random

# Metadata
__author__ = "Jake Magness"
__email__ = "C18Jake.Magness@usafa.edu"
__date__ = "1 Dec 2016"

# Constants for the state of the game
CONTINUE_PLAYING = 0
GAME_OVER = 1


class RackoGame:

    # -----------------------------------------------------------------
    def __init__(self, number_of_players):
        """
        Initializes the RACKO game.  Sets up players, decks of cards, the game status.
        """
        # Get the game status
        self.status = 0
        self.number_of_players = number_of_players

        # Get the number of players
        num_dealer = self.number_of_players

        # Create initial game deck
        self.deck_of_cards = Cards(1, 20 + self.number_of_players*10)
        self.deck_of_cards.shuffle()

        # Set up player objects in the players list
        self.players = [0]*self.number_of_players
        for i in range(self.number_of_players):
            name = i + 1
            self.players[i] = Player(name)
            hand = Cards()
            for n in range(10):
                card = self.deck_of_cards.remove_top_card()
                hand.add_to_end(card)

            self.players[i].set_hand(hand)


        # Designate a dealer
        self.dealer = random.randrange(0, num_dealer)

        # Designate active player
        if self.dealer == self.number_of_players - 1:
            self.active_player = 0
        else:
            self.active_player = self.dealer + 1

        # Draw Pile
        self.draw_pile = self.deck_of_cards

        # Discard Pile
        self.discard_pile = Cards(0, 0)
        self.discard_pile.add_to_top(self.draw_pile.remove_top_card())

    # -----------------------------------------------------------------
    def __str__(self):
        """
        A function that can be used to display the current status of the game
        :return: Returns the game status
        """
        return "The current dealer is: {}.\nThe active player is {}.\nThe card on top is the discard pile is {}.\n" \
               "The card on top of the draw_pile is {}".format(self.players[self.dealer].name,
                                                               self.players[self.active_player].name,
                                                               self.draw_pile.get_top_card(),
                                                               self.discard_pile.get_top_card())

    # -----------------------------------------------------------------
    def pass_out_cards(self):
        """
        Function to pass out cards.  This was taken care of in the game setup
        :return: Nothing
        """
        # self.deck_of_cards.cards_list[:10]
        pass

    # -----------------------------------------------------------------
    def save_game(self, name):
        """
        Saves the game, this function was provided to students
        :return: Nothing
        """
        filename = name
        with open(filename, "wb") as outfile:
            pickle.dump(self, outfile)

    # -----------------------------------------------------------------
    def update_draw_pile(self):
        """
        Updates the draw pile.
        :return: Nothing
        """
        if self.draw_pile == []:
            self.discard_pile.remove_top_card()
            self.draw_pile = self.discard_pile
            self.discard_pile = []

    # -----------------------------------------------------------------
    def active_player_takes_their_turn(self):
        """
        A function that is called for each active player.  This allows the player to take their turn
        :return: Nothing
        """
        print("{}, it is now your turn.".format(self.players[self.active_player].name))
        move = get_draw_card_choice(self.discard_pile.get_top_card())
        discard = self.discard_pile.get_top_card()
        # Determining how to act based on user input
        if move == -4:
            self.save_game()
        elif move == -1:
            self.status = GAME_OVER

        # Discard Pile move
        elif move == -6:
            remove_card = get_discard_choice(self.players[self.active_player].hand.cards_list,
                                             discard)
            self.discard_pile.add_to_top(remove_card)
            if remove_card == -1:
                self.status = GAME_OVER
            else:
                position_to_remove = self.players[self.active_player].hand.find(remove_card)
                self.players[self.active_player].hand.replace(position_to_remove, discard)

        # Draw Pile move
        else:
            draw = self.draw_pile.get_top_card()
            del self.draw_pile.cards_list[0]
            remove_card = get_discard_choice(self.players[self.active_player].hand.cards_list,
                                             draw)
            self.discard_pile.add_to_top(remove_card)
            if remove_card == -1:
                self.status = GAME_OVER
            else:
                position_to_remove = self.players[self.active_player].hand.find(remove_card)
                if position_to_remove == -1:
                    self.discard_pile.add_to_top(draw)
                else:
                    self.players[self.active_player].hand.replace(position_to_remove, draw)

        racko = self.active_players_hand_is_racko()
        if racko == True:
            self.round_is_finished()

    # -----------------------------------------------------------------
    def update_player_scores(self):
        """
        Updates all players scores.
        :return: Nothing
        """
        # For loop that iterates through all players then adds up their hand value.
        for n in range(self.number_of_players):
            i = 0
            score = 5
            ok_to_compare_cards = True
            while i < 9 and ok_to_compare_cards:

                # Checks to see if the current value + 1 and the next card are equivalent, meaning they are in order
                if self.players[n].get_hand().get_card_at(i) < (self.players[n].get_hand().get_card_at(i+1)):
                    score += 5
                else:
                    ok_to_compare_cards = False

                i += 1

            if score == 50:
                self.players[n].score += 75
            else:
                self.players[n].score += score

    # -----------------------------------------------------------------
    def active_players_hand_is_racko(self):
        """
        Checks to see if the active player has a RACKO hand
        :return: True or False whether the player has a RACKO hand or not
        """
        racko = False
        count = 1
        i = 0

        # Loop that checks all elements of the hand to see if all numbers are in order.
        while i < 9:

            if self.players[self.active_player].get_hand().get_card_at(i) < (self.players[self.active_player].
                                                                                     get_hand().get_card_at(i+1)):
                count += 1

            i += 1

        if count == 10:
            racko = True

        return racko

    # -----------------------------------------------------------------
    def player_hand_value(self, which_player):
        """
        Gets the value of the player's hand.
        :param which_player: The player that you want to find the value of
        :return: The players score
        """
        return self.players[which_player].score

    # -----------------------------------------------------------------
    def round_is_finished(self):
        """
        Displas the scores and calls the reset next round function which prepares the game for the next round
        :return: Nothing
        """
        self.update_player_scores()
        self.reset_for_next_round()
        x = self.display_all_current_scores()
        x += self.display_round_winner()
        return x
    # -----------------------------------------------------------------
    def reset_for_next_round(self):
        """
        Resets all parameters for the next round and displays player scores
        :return: None
        """
        # If dealer doesn't equal the last player, add 1.  If it does equal last player, move back to first player
        if self.dealer != self.number_of_players - 1:
            self.dealer += 1
        else:
            self.dealer = 0

        # Same principle for active player as for dealer
        if self.dealer == self.number_of_players - 1:
            self.active_player = 0
        else:
            self.active_player = self.dealer + 1

        # Set up new deck of cards for next round
        self.deck_of_cards = Cards(1, 20 + self.number_of_players*10)
        self.deck_of_cards.shuffle()

        for i in range(self.number_of_players):
            self.players[i].hand.cards_list = self.deck_of_cards.cards_list[:10]
            del self.deck_of_cards.cards_list[0:10]

        # Draw Pile
        self.draw_pile = self.deck_of_cards

        # Discard Pile
        self.discard_pile = Cards(0, 0)

    # -----------------------------------------------------------------

    def display_all_current_scores(self):
        """
        Displays the scores of all players
        :return: Nothing
        """
        text = "The round is over, the scores so far are as follows:\n"
        for x in range(self.number_of_players):
            text += "Player {}'s score is: {}\n".format(self.players[x].name, self.players[x].get_score())

        return text

    # -----------------------------------------------------------------

    def display_round_winner(self):
        """
        Displays the player that won the round
        :return: Nothing
        """
        max = 0
        round_winner_index = 0
        for i in range(self.number_of_players):
            if self.players[i].get_score() > max:
                max = self.players[i].get_score()
                round_winner_index = i


        if max < 500:
            x = "Player {} Won the Round! \n".format(self.players[round_winner_index].get_name())
            x += "The cards have now been shuffled, the dealer is rotated and a new active player assigned."
        else:
            x = "Player {} Won the Round! \n".format(self.players[round_winner_index].get_name())

        if max >= 500:
            self.status = GAME_OVER
            x = self.display_final_results()

        return x
    # -----------------------------------------------------------------

    def display_final_results(self):
        """
        Displays the final results for the game
        :return: Nothing
        """
        x = "\nThe game is over!\n"
        x += "The final results for the game are as follows:\n"
        for i in range(self.number_of_players):
            x += "{}'s score is: {}".format(self.players[i].get_name(), self.players[i].get_score()) + "\n"

        return x
    # -----------------------------------------------------------------
    def play(self):
        """  Play the game until the user quits or someone wins. """
        while self.status == CONTINUE_PLAYING:

            self.active_player_takes_their_turn()
            if self.status == CONTINUE_PLAYING:

                # Check for a winner
                if self.active_players_hand_is_racko():
                    self.round_is_finished()
                else:
                    # Switch active player
                    self.active_player = (self.active_player + 1) % self.number_of_players

        # The game is over; display the status of the game when it ended.
        self.display_final_results()
# ---------------------------------------------------------------------

    def load_saved_racko_game(self, filename):
        """
        Loads a saved RACKO game, this function was provided to students
        :return: Returns the RACKO Game
        """
        with open(filename, "rb") as infile:
            racko_game = pickle.load(infile)

        return racko_game
# =====================================================================


def main():
    """ Play RACK-O """
    start = get_how_to_start_game()
    RackoGame.status = start

    if start == NEW_GAME:
        game = RackoGame()
        game.play()
    elif start == LOAD_PREVIOUS_GAME:
        # game = load_saved_racko_game()
        game.play()
    else:  # start == QUIT_GAME
        exit()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function

if __name__ == "__main__":
    main()