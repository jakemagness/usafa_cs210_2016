#!/usr/bin/env python

"""
C2C Jake Magness
4 October 2018

Creates functions that test all the functions that will be used to create the Racko game.

Documentation Statement: None.

"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules
from PEX2_Racko_functions import create_deck, get_number_of_players, shuffle_deck, pass_out_cards, remove_top_card, append_card_to_deck, player_hand_value

# Metadata
__author__ = "Jake Magness"
__email__ = "C18Jake.Magness@usafa.edu"
__date__ = "Sept 28, 2016"


# ---------------------------------------------------------------------
def correct_cards_in_deck(deck):
    """
    Verify that a deck of cards contains the correct card values
    :param deck: [int] a list of integers
    :return: Boolean True if each card appears exactly once in the deck
    """
    # There must be one card of each number in the deck. Count the
    # number of times each card is found.
    counters = [0] * (len(deck)+1)
    for card_value in deck:
        counters[card_value] += 1

    # Now verify that each card was found only once
    valid_deck = True
    for card_value in range(1, len(deck)+1):
        if counters[card_value] == 0:
            print("Test failed: card {} is missing from the deck"
                  .format(card_value))
            valid_deck = False
        elif counters[card_value] > 1:
            print("Test failed: card {} is in the deck {} times".
                  format(card_value, counters[card_value]))
            valid_deck = False

    return valid_deck


# ---------------------------------------------------------------------
def verify_create_deck():
    """
    Verify the correctness of the create_deck() function
    """
    # Test for valid inputs
    deck = create_deck(2)

    if not isinstance(deck, list):
        print("Test failed: the return value of create_deck must be a list")

    if len(deck) != 40 or not correct_cards_in_deck(deck):
        print("Test failed: 2 player deck is incorrect.")
        print("The invalid deck is ", deck)

    deck = create_deck(3)
    if len(deck) != 50 or not correct_cards_in_deck(deck):
        print("Test failed: 3 player deck is incorrect.")
        print("The invalid deck is ", deck)

    deck = create_deck(4)
    if len(deck) != 60 or not correct_cards_in_deck(deck):
        print("Test failed: 4 player deck is incorrect.")
        print("The invalid deck is ", deck)

    # Test for invalid inputs
    for number_players in [0, 1, 5, 6, 7]:
        deck = create_deck(number_players)
        if deck is not None:
            print("Test failed: {} players did not return as an error"
                  .format(number_players))

# ---------------------------------------------------------------------

def verify_number_of_players():
    """
    Verify the correctness of the number_of_players function
    """

    # Input number of players as 2
    play = get_number_of_players()
    if not isinstance(play, int):
        print("Test failed: the return value of number_of_players must be an integer")

    # Input the number of players as 2
    play = get_number_of_players()
    if play != 2:
        print("Test failed: Function accepted player number that was incorrect")

    # Input the number of players as 3
    play = get_number_of_players()
    if play != 3:
        print("Test failed: Function accepted player number that was incorrect.")

    # Input the number of players as 4
    play = get_number_of_players()
    if play != 4:
        print("Test failed: Function accepted player number that was incorrect.")

    # Input the number of players as "quit"
    play = get_number_of_players()
    if play != -1:
        print("Test failed: Function should return -1 when user inputs quit.")

    # Input number of players that isn't 2, 3, or 4.  If the function does not return none is it incorrect
    play = get_number_of_players()
    if play is not None:
        print("Test failed: Function should return None for numbers that are not 2, 3 or 4.")

# ---------------------------------------------------------------------

def verify_shuffle_deck():
    """
    Creates a new deck for a number of players.  Passes deck to the shuffle_deck function.  If shuffle_deck returns the
    deck, the test has failed.

    """
    # To test if deck is properly shuffled for different numbers of players

    for i in range(3):

        deck = create_deck(i + 2)
        new_deck = shuffle_deck(deck)

        # Test for the return of a list
        if not isinstance(new_deck, list):
            print("Test failed: the return value of shuffle_deck must be a list")

        # Test if the deck was shuffled.
        if deck == new_deck:
            print("Test failed: The deck was not reordered by the function.")

# ---------------------------------------------------------------------

def verify_pass_out_cards():
    """
    Verifies if cards are passed out to each player.  The function should return a list of lists
    """

    # Need to test for all lengths of decks
    for i in range(3):

        deck = create_deck(i + 2)
        shuffled_deck = shuffle_deck(deck)
        condition = pass_out_cards(shuffled_deck, i+2)

        if not isinstance(condition, list):
            print("Test failed: the return value of pass_out_cards must be a list of lists")

        # Test to make sure that all players have 10 cards
        if len(condition[i]) != 10:
            print("Test failed: all players do not have 10 cards")

# ---------------------------------------------------------------------

def verify_remove_top_card():
    """
    Verifies that a card was removed from the top of the deck
    """

    # Conditions for the tests
    deck = create_deck(2)
    condition = pass_out_cards(deck, 2)
    hand1 = condition[0][0]
    card = remove_top_card(condition[0])


    if not isinstance(card, int):
        print("Test failed: The return value of remove_top_card is not an integer")

    if condition[0] == hand1:
        print("Test failed: Did not remove top card")

    array = []
    condition = remove_top_card(array)

    if condition != -1:
        print("Test failed: Function did not return -1 for an empty array")

# ---------------------------------------------------------------------

def verify_append_card_to_deck():

    # Variables for the test
    deck = create_deck(2)
    condition = pass_out_cards(deck, 2)
    hand1 = condition[0]

    # Clone list to keep original values when changing hand1 to compare later
    hand2 = hand1[:]

    append_card_to_deck(hand1, 5)


    if not isinstance(hand1, list):
        print("Test failed: The return value of remove_top_card is not an integer")

    if len(hand2) == len(hand1):
        print("Test failed: card was not appended to deck")

# ---------------------------------------------------------------------

def verify_player_hand_value():

    # Variables for the test
    # First deck should return value of 75
    winningdeck = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    # Should return value of  25
    scoredeck1 = [1, 2, 3, 4, 5, 7, 7, 8, 9, 10]
    # Should return value of 5
    scoredeck2 = [1, 3, 4, 5, 6, 7, 7, 8, 9, 1]



    if not isinstance(player_hand_value(winningdeck, False), int):
        print("Test failed: The return value of player_hand_value is not an integer")

    if player_hand_value(winningdeck, True) != 75:
        print("Test failed: Scored a winning deck incorrectly.")

    if player_hand_value(scoredeck1, False) != 25:
        print("Test failed: Scored a deck incorrectly")

    if player_hand_value(scoredeck2, False) != 5:
        print("Test failed: Scored a deck incorrectly")

# ---------------------------------------------------------------------
def main():
    """
    Execute the validation tests for the RACK-O functions.  All test functions call the function multiple times
    to test for different conditions.
    """

    # Verify create deck has had errors since the code was given to me.
    #  My understanding was that it was correct as given.
    verify_create_deck()
    verify_number_of_players()
    verify_shuffle_deck()
    verify_pass_out_cards()
    verify_remove_top_card()
    verify_append_card_to_deck()
    verify_player_hand_value()


if __name__ == "__main__":
    main()
